<?php

class teacherListController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$teachers = teacher::paginate(5);
		$results = teacher::all();

		// load the view and pass the nerds
		return View::make('admin.teacherlist.index')->with('teachers', $teachers)->with('results', $results);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.teacherlist.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$messages 		= array('required_if' => 'The :attribute field is required.');
		$rules 			= array(
						'username'=>'required',
						'firstname' 	=>'required',
						'lastname'		=>'required'
					);

		$validator				= Validator::make(Input::all(),$rules,$messages);

		if($validator->fails()){
			return Redirect::to('/teacherlist/create')->withErrors($validator)->withInput();
		}else{
			
			$teacher 						= new teacher;
			$teacher->username				= Input::get('username');
			$teacher->firstname				= Input::get('firstname');
			$teacher->lastname				= Input::get('lastname');
			$teacher->position				= Input::get('position');
			$teacher->advisory				= Input::get('advisory');
			$teacher->units					= Input::get('units');
			$teacher->contact		= Input::get('contact');
			$teacher->subjectmajor				= Input::get('subjectmajor');
			$teacher->birthday				= Input::get('birthday');
			$teacher->password 				= Hash::make($teacher->birthday);
			$teacher->save();


			Session::flash('success','Account created!');
			return Redirect::to('/teacherlist');
	}
}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$teacher = teacher::find($id);

		// show the view and pass the nerd to it
		return View::make('admin.teacherlist.show')
			->with('teacher', $teacher);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$teacher = teacher::find($id);

		// show the edit form and pass the nerd
		return View::make('admin.teacherlist.edit')
			->with('teacher', $teacher);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$messages 		= array('required_if' => 'The :attribute field is required.');
		$rules 			= array(
						'username'=>'required',
						'firstname' 	=>'required',
						'lastname'		=>'required'
					);

		$validator				= Validator::make(Input::all(),$rules,$messages);

		if($validator->fails()){
			return Redirect::to('/teacherlist/create')->withErrors($validator)->withInput();
		}else{
			$month = Input::get('month');
			$day = Input::get('day');
			$year = Input::get('year');
			$teacher 						= Teacher::find($id);
			$teacher->username				= Input::get('username');
			$teacher->firstname				= Input::get('firstname');
			$teacher->lastname				= Input::get('lastname');
			$teacher->position				= Input::get('position');
			$teacher->advisory				= Input::get('advisory');
			$teacher->contact		= Input::get('contact');
			$teacher->units					= Input::get('units');
			$teacher->subjectmajor				= Input::get('subjectmajor');
			$teacher->birthday				= $month."/".$day."/".$year;
			$teacher->save();


			Session::flash('success','Updated!');
			return Redirect::to('/teacherlist');
		
	}
}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$teacher = Teacher::find($id);
		$teacher->delete();



		// redirect
		Session::flash('message', 'Successfully deleted the nerd!');
		return Redirect::to('/teacherlist');
	}


}
