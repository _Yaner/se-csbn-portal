<?php

class profileAdminController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function showSignup(){
		
		return View::make('layouts.admin.admin');
	}



	public function showstudentlist(){
		$students  = Student::orderBy('firstname','asc')->get();
		$studentNumAscs  = Student::orderBy('studentnumber','asc')->get();
		return View::make('layouts.admin.studentlist')->with('students',$students)->with('studentNumAscs',$studentNumAscs);
	}

	public function showlevels(){
		$students  = Student::all();
		
		return View::make('layouts.admin.levels')->with('students',$students);
	}

	public function showlist(){
		$value			= Input::get('value');
		
		return View::make('layouts.admin.studentlist')->with('value',$value);
	}

	public function viewStudent()
	{
		
		$user					= User::all();
		$students                = Student::all();
		return View::make('layouts.student.profile')->with('user',$user)->with('students',$students);
		
	}

	public function CreateStudent(){
		$messages 		= array('required_if' => 'The :attribute field is required.');
		$rules 			= array(
						'studentnumber'=>'required',
						'firstname' 	=>'required',
						'lastname'		=>'required',
						'gender'		=>'required',
						
						
					);

		$validator				= Validator::make(Input::all(),$rules,$messages);

		if($validator->fails()){
			return Redirect::to('/students/addStudent')->withErrors($validator)->withInput();
		}else{
			$month = Input::get('month');
			$day = Input::get('day');
			$year = Input::get('year');
			$student 						= new Student;
			$user 						= new User;
			$student->studentnumber			= Input::get('studentnumber');
			$student->firstname				= Input::get('firstname');
			$student->level					= Input::get('level');
			$student->lastname				= Input::get('lastname');
			$student->gender 				= Input::get('gender');
			$student->birthday				= $month."/".$day."/".$year;
			$student->nationality 			= Input::get('nationality');
			$student->address 				= Input::get('address');
			$student->contact 				= Input::get('contact');
			$student->email					= Input::get('email');
			$student->father 				= Input::get('father');
			$student->mother				= Input::get('mother');
			//$student->parentadd 			= Input::get('parentadd');
			$student->parentcontact 		= Input::get('parentcontact');
			$student->parentsms 			= Input::get('parentsms');
			//$student->guardian 				= Input::get('guardian');
			//$student->guardianadd			= Input::get('guardianadd');
			//$student->guardiancontact 		= Input::get('guardiancontact');
			//$student->guardiansms			= Input::get('guardiansms');
			$student->save();

			
			$user->username             = Input::get('studentnumber');
			$user->password             = Hash::make(Input::get('birthday'));
			$user->profile_id             = $student->_id;
			$user->save();


			Session::flash('success','Account created!');
			return Redirect::to('/students/addStudent');
		}
	}
	public function index()
	{
		return View::make('layouts.student.profile')->with('student',$student);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
