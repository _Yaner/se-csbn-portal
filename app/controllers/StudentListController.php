<?php

class StudentListController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */ 
	public function index()
	{
		// get all the nerds
		$students = Student::paginate(5);
		$results = Student::all();
		
		// load the view and pass the nerds
		return View::make('admin.studentlist.index')
			->with('students', $students)->with('results', $results);


	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.studentlist.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$messages 		= array('required_if' => 'The :attribute field is required.');
		$rules 			= array(
						'studentnumber'=>'required | unique:student | numeric',
						'firstname' 	=>'required',
						'lastname'		=>'required',
						'birthday'		=>'required',
						'level'			=>'required',
						'gender'		=>'required',
						'parentcontact' =>'numeric',
						'contact'		=>'numeric'
					);

		$validator				= Validator::make(Input::all(),$rules,$messages);

		if($validator->fails()){
			return Redirect::to('/studentlist/create')->withErrors($validator)->withInput();
		}else{
			// $month = Input::get('month');
			// $day = Input::get('day');
			// $year = Input::get('year');
			$student 						= new Student;
			$user 							= new User;
			$student->studentnumber			= Input::get('studentnumber');
			$student->firstname				= Input::get('firstname');
			$student->level					= Input::get('level');
			$student->lastname				= Input::get('lastname');
			$student->gender 				= Input::get('gender');
			$student->birthday				= Input::get('birthday');
			$student->nationality 			= Input::get('nationality');
			$student->address 				= Input::get('address');
			$student->contact 				= Input::get('contact');
			$student->email					= Input::get('email');
			$student->father 				= Input::get('father');
			$student->mother				= Input::get('mother');
			$student->parentcontact 		= Input::get('parentcontact');
			$student->parentsms 			= Input::get('parentsms');
			$student->save();

			
			$user->username             = Input::get('studentnumber');
			$user->password             = Hash::make($student->birthday);
			$user->profile_id             = $student->_id;
			$user->save();


			Session::flash('success','Account created!');
			return Redirect::to('/studentlist');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$student = Student::find($id);

		// show the view and pass the nerd to it
		return View::make('admin.studentlist.show')
			->with('student', $student);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$student = Student::find($id);

		// show the edit form and pass the nerd
		return View::make('admin.studentlist.edit')
			->with('student', $student);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$messages 		= array('required_if' => 'The :attribute field is required.');
		$rules 			= array(
						'studentnumber'=>'required | numeric',
						'firstname' 	=>'required',
						'lastname'		=>'required',
						'gender'		=>'required'
					);

		$validator				= Validator::make(Input::all(),$rules,$messages);

		if($validator->fails()){
			return Redirect::to('/studentlist/'.$id.'/edit')->withErrors($validator)->withInput();
		}else{
			// $month = Input::get('month');
			// $day = Input::get('day');
			// $year = Input::get('year');
			$student 						= Student::find($id);
			$student->studentnumber			= Input::get('studentnumber');
			$student->firstname				= Input::get('firstname');
			$student->level					= Input::get('level');
			$student->lastname				= Input::get('lastname');
			$student->gender 				= Input::get('gender');
			$student->birthday				= Input::get('birthday');
			$student->nationality 			= Input::get('nationality');
			$student->address 				= Input::get('address');
			$student->contact 				= Input::get('contact');
			$student->email					= Input::get('email');
			$student->father 				= Input::get('father');
			$student->mother				= Input::get('mother');
			$student->parentcontact 		= Input::get('parentcontact');
			$student->parentsms 			= Input::get('parentsms');
			$student->save();

			
			


			Session::flash('success','Success');
			return Redirect::to('/studentlist');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$student = Student::find($id);
		$account   = User::where('profile_id','=',$id);
		$student->delete();
		$account->delete();


		// redirect
		Session::flash('message', 'Successfully deleted the nerd!');
		return Redirect::to('/studentlist');
	}


}
