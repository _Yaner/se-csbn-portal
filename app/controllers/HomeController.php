<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	| 
	*/

	public function search()
	{
		$term = Input::get('term');
		//$users = array();
		if($term == null)
		{
			$students = Student::paginate(5);
			$results = Student::all();
		
		// load the view and pass the nerds
		return View::make('admin.studentlist.index')->with('students', $students)->with('results', $results);
		}
		$parts = explode(', ', $term);
		//dd($parts);
		$result = Student::where('lastname','=',$parts[0])->where('firstname','=',$parts[1])->first();
		//dd($results);

		$students = Student::all();
		return View::make('admin.studentlist.search')->with('result',$result)->with('students',$students);
	 
	}
	public function searchTeacher()
	{
		$term = Input::get('term');
		//$users = array();
		if($term == null)
		{
			$teachers = Teacher::paginate(5);
			$results = Teacher::all();
		
		// load the view and pass the nerds
		return View::make('admin.teacherlist.index')->with('teachers', $teachers)->with('results', $results);
		}
		$parts = explode(', ', $term);
		//dd($parts);
		$result = Teacher::where('lastname','=',$parts[0])->where('firstname','=',$parts[1])->first();
		//dd($results);

		$teachers= Teacher::all();
		return View::make('admin.teacherlist.search')->with('result',$result)->with('teachers',$teachers);
	 
	}


	public function index()
	{
		return View::make('layouts.admin.admin');
	}

	public function showWelcome()
	{
		return View::make('hello');
	}

	public function showLogin()
	{
		// show the form
		return View::make('layouts.login.login');
	}

	public function doAdminLogin()
	{
		$rules 				= array(
							'username'    => 'required',
							'password' => 'required'
						);
		$validator 			= Validator::make(Input::all(), $rules);

		if($validator->fails()){
			return Redirect::to('/admin/login')->withErrors($validator)->withInput();
		}else{

			$userdata 		= array(
							'username' 	=> Input::get('username'),
							'password' 	=> Input::get('password')
						); 

			$user 			= Admin::where('username', $userdata['username'])->first();
			if($user && Hash::check($userdata['password'],$user->password)){
				Session::put('admin', $user);
				return Redirect::to('/studentlist');
			} else {	 	

				return Redirect::to('/admin/login')->withErrors('Wrong Username/Password')->withInput();
			}

		}
	
	}

	public function doTeacherLogin()
	{
		$rules 				= array(
							'username'    => 'required',
							'password' => 'required'
						);
		$validator 			= Validator::make(Input::all(), $rules);

		if($validator->fails()){
			return Redirect::to('/teacher/login')->withErrors($validator)->withInput();
		}else{

			$userdata 		= array(
							'username' 	=> Input::get('username'),
							'password' 	=> Input::get('password')
						); 

			$user 			= Teacher::where('username', $userdata['username'])->first();
			if($user && Hash::check($userdata['password'],$user->password)){
				Session::put('teacher', $user);
				return Redirect::to('/teacher');
			} else {	 	

				return Redirect::to('/teacher/login')->withErrors('Wrong Username/Password')->withInput();
			}

		}
	
	}



	public function doLogin()
	{
		$rules 				= array(
							'username'    => 'required',
							'password' => 'required'
						);
		$validator 			= Validator::make(Input::all(), $rules);

		if($validator->fails()){
			return Redirect::to('/student/login')->withErrors($validator)->withInput();
		}else{

			$userdata 		= array(
							'username' 	=> Input::get('username'),
							'password' 	=> Input::get('password')
						); 

			$user 			= User::where('username', $userdata['username'])->first();
			if($user && Hash::check($userdata['password'],$user->password)){
				Session::put('student', $user);
				 return Redirect::to('/profile');
			} else {	 	

				return Redirect::to('/student/login')->withErrors('Wrong Username/Password')->withInput();
			}

		}
	
	}

	public function viewStudent()
	{
		if(Session::has('student')){
		$session 				= Session::get('student');
		$user					= User::where('id','=',$session->id);
		$students                = Student::all();
		return View::make('layouts.student.profile')->with('user',$user)->with('students',$students);
		}
	}

	public function changepass()
	{
		if(Session::has('student')){
		$session 				= Session::get('student');
		$user					= User::where('id','=',$session->id);
		$students                = Student::all();
		return View::make('layouts.student.changepass')->with('user',$user)->with('students',$students);
		}
	}

	public function adminchangepass()
	{
		if(Session::has('admin'))
		{
		$session 				= Session::get('admin');
		$admin					= Admin::where('id','=',$session->id);
		
		return View::make('admin.adminchangepass')->with('admin',$admin);
		}
	}

	public function adminupdatepass()
	{
		$rules 				= array(
							'Old-password'    => 'required',
							'New-password' => 'required |min:6 ',
							'Confirm-password' =>'required'
						);
		$validator 			= Validator::make(Input::all(), $rules);
		$message ="All Fields required";

		if($validator->fails()){
			return Redirect::to('/adminchangepass')->withErrors($message)->withInput();
		}else{

			$userdata 		= array(
							'old' 	=> Input::get('Old-password'),
							'new' 	=> Input::get('New-password'),
							'confirm' 	=> Input::get('Confirm-password')
						); 
			$session 				= Session::get('admin');

			$admin			= Admin::where('_id','=',$session->id)->first();
			
			if($admin && Hash::check($userdata['old'],$admin->password)){
				if($userdata['new']==$userdata['confirm'])
				{
					$admin->password   = Hash::make(Input::get('New-password'));
					$admin->save();
					$message = "Success";
					return Redirect::to('/adminchangepass')->with('message',$message)->withInput();
				}else{
					$error_message = "Password did not match";
					return Redirect::to('/adminchangepass')->withErrors($error_message)->withInput();
				}
				
			} else {	 	
				$error_message = "Incorrect Old Password";
				return Redirect::to('/adminchangepass')->withErrors($error_message)->withInput();
			}

		}
	}

	public function editpass()
	{
		$rules 				= array(
							'Old-password'    => 'required',
							'New-password' => 'required',
							'Confirm-password' =>'required'
						);
		$validator 			= Validator::make(Input::all(), $rules);
		$message ="All Fields required";

		if($validator->fails()){
			return Redirect::to('/changepass')->withErrors($message)->withInput();
		}else{

			$userdata 		= array(
							'old' 	=> Input::get('Old-password'),
							'new' 	=> Input::get('New-password'),
							'confirm' 	=> Input::get('Confirm-password')
						); 
			$session 				= Session::get('student');

			$user 			= User::where('_id','=',$session->id)->first();
			
			if($user && Hash::check($userdata['old'],$user->password)){
				if($userdata['new']==$userdata['confirm'])
				{
					$user->password   = Hash::make(Input::get('New-password'));
					$user->save();
					$message = "Success";
					return Redirect::to('/changepass')->with('message',$message)->withInput();
				}else{
					$error_message = "Password did not match";
					return Redirect::to('/changepass')->withErrors($error_message)->withInput();
				}
				
			} else {	 	
				$error_message = "Incorrect Old Password";
				return Redirect::to('/changepass')->withErrors($error_message)->withInput();
			}

		}

	}

	public function geteditcontact()
	{
		if(Session::has('student')){
		$session 				= Session::get('student');
		$user					= User::where('id','=',$session->id);
		$students                = Student::all();
		return View::make('layouts.student.editcontact')->with('user',$user)->with('students',$students);
		}
	}

	public function savecontact()
	{

		$rules 				= array(
							'email'    => 'email'
						);
		$validator 			= Validator::make(Input::all(), $rules);

		if($validator->fails()){
			return Redirect::to('/editcontact')->withErrors($validator)->withInput();
		}else{

			$userdata 		= array(
							'email' 	=> Input::get('email'),
							'contact' 	=> Input::get('contact')
						); 
			$session 				= Session::get('student');

			$user 			= User::where('_id','=',$session->id)->first();
			$student        = Student::where('_id','=',$user->profile_id)->first();
			$student->email 		= 	(Input::get('email'));
			$student->contact 		= 	(Input::get('contact'));
			$student->save();
			$message = "Saved Changed";
			return Redirect::to('/editcontact')->with('message',$message)->withInput();
				
			
		}

	}

	public function viewgrades()
	{
		return View::make('layouts.student.grades');
	}

	public function adminviewgrades()
	{
		return View::make('admin.studentlist.studentviews.grades');
	}
	public function adminviewattendance()
	{
		return View::make('admin.studentlist.studentviews.attendance');
	}
	public function adminviewlogs()
	{
		$attendances 	= 	Attendance::all();
		//dd($attendances);
		return View::make('admin.studentlist.studentviews.logs')->with('attendances', $attendances);
	}
	public function adminstudentreset()
	{
		$message 	= "Password Reset Success! Password set to default MM/DD/YYYY";
		return Redirect::back()->with('success',$message);
	}
	public function adminviewaddnote()
	{
		return View::make('admin.studentlist.studentviews.addnote');
	}

	public function sendnote()
	{

		$receiver =  Input::get('receiver');
		$number			=	"+639495273956";
		$sender		= 	(Input::get('sender'));
		$title	= 	(Input::get('title'));
		$content 		= 	(Input::get('content'));
		$sendvia		= Input::get('sendvia');
		if($sendvia == 'sms')
		{
		Twilio::to($number)->message("\n"."From: ".$sender."\n"."Title: ".$title."\n".$content);
		$message 	=	"Add Note Success!";
		return Redirect::back()->with('success',$message);
		}else
		{
			Mail::send('emails.auth.email',array('name'=>Input::get('receiver'),'content'=>$content,'sender'=>$sender), function($message) {
			    $message->to('janinejaner@gmail.com', Input::get('receiver'))->subject(Input::get('title'));
			});
			 $mes	=	"Add Note Success!";
			return Redirect::back()->with('success',$mes);
		}
		
	}

	public function tadminviewgrades()
	{
		return View::make('teacher.studentlist.studentviews.grades');
	}
	public function tadminviewattendance()
	{
		$attendances 	= 	Attendance::all();
		return View::make('teacher.studentlist.studentviews.attendance');
	}
	public function tadminviewlogs()
	{
		$attendances 	= 	Attendance::all();
		
		return View::make('teacher.studentlist.studentviews.logs')->with('attendances', $attendances);
	}
	public function tadminstudentreset()
	{
		$message 	= "Password Reset Success! Password set to default MM/DD/YYYY";
		return Redirect::back()->with('success',$message);
	}
	public function tadminviewaddnote()
	{
		return View::make('teacher.studentlist.studentviews.addnote');
	}








}
