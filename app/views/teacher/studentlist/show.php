
<!DOCTYPE html>
<html>
<head>
	<title>CSBN PORTAL | Admin</title>
	<link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link rel="stylesheet" href="/css/createstudent.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="/assets/js/vendor/modernizr.js"></script>
	 <link rel="icon" href="http://d15dxvojnvxp1x.cloudfront.net/assets/favicon.ico">
	<link rel="stylesheet" type="text/css" media="all" href="css/styles.css">
	 
</head>
<body>

<div id="header">
  <div class="container">
    <img src="/img/CSBNLogo.png">
      <span id="CSBN">CSBN</span>
      <span id="portal">Portal</span>
      <p id="colegio">Colegio de San Bartolome de Novaliches</p> <?php $users = Session::get('teacher'); $teacher = Teacher::find($users->_id); ?>
          <a href="#" id="admins" data-dropdown="admin" class="tiny button dropdown"><?php echo $teacher->firstname." ".$teacher->lastname?></a><br>
        <ul id="admin" data-dropdown-content class="f-dropdown">
          <li><a href="/teacherchangepass">Change Password</a></li>
          <li><a href="/teacher-logout">Logout</a></li>
        </ul>
    </img>
  </div>
</div>
<div id="line"></div>

<ul class="side-nav">
  <li><a href="/teacher">Student</a></li>
  <li><a href="/teacherlist">Teacher</a>
</ul>
<div id="maincontent">
 <span id="link" style="font-size:8pt">List of Students > Student Profile</span>
 <br/>
 <a class="button tiny secondary" type="button" href="/teacher"> < Back</a>
 <br>
<?php if(Session::has('message')){?>
	<div class="alert alert-info"><?php Session::get('message');?></div>
<?php }?>

<!-- if there are creation errors, they will show here -->
<?php if($errors->has()){?>
              <div data-alert class="alert-box alert">
                <?php foreach ($errors->all() as $error) { ?>
                  <li><?php echo $error; ?></li>
                <?php } ?>
              </div>
            <?php } ?> 
            <?php  $message = Session::get('success'); if($message!=null){?>
            <div data-alert class="alert-box success">
                
                  <li><?php echo $message;?></li>
              
              </div>
              <?php }?>


	<div class="jumbotron text-center">
		 <table class="radius center large-10 medium-8 small-6 large-centered columns">
      <tr>
        <td colspan="4">
          <div id="image" class="left"></div>
          <div class="left">
            <span id="fullname"><?php echo $student->firstname; echo $student->lastname;?></span><br />
            <span id="studentid"><?php echo $student->studentnumber;?></span><br />
            <span id="section">4th year - Prudence</span>
          </div>

            <div class="button-bar">
              <u class="button-group [radius round]">
                <li><a href="/teacher/<?php echo $student->id?>/grades" class="[tiny small large] button [alert success secondary] [disabled]">View Grades</a></li>
                <li><a href="/teacher/<?php echo $student->id?>/attendance" class="[tiny small large] button [alert success secondary] [disabled]">View Attendance</a></li>
                <li><a href="/teacher/<?php echo $student->id?>/addNote" class="[tiny small large] button [alert success secondary] [disabled]">Add Notes</a></li>
                <li><a href="/teacher/<?php echo $student->id?>/reset" class="[tiny small large] button [alert success secondary] [disabled]">Reset Password</a></li>
              </u>
            </div>
        </td>
      </tr>
      <tr>
        <th colspan="4"></th>
      </tr>
      <tr>
        <td>Name:</td>
        <td><?php echo $student->firstname; echo " ".$student->lastname;?></td>
        <td>Gender:</td>
        <td><?php echo $student->gender;?></td>
      </tr>
      <tr>
        <td>Birthday:</td>
        <td><?php echo $student->birthday;?></td>
        <td>Nationality:</td>
        <td><?php echo $student->nationality;?></td>
      </tr>
      <tr>
        <td>Address:</td>
        <td colspan="3"><?php echo $student->address;?></td>
      </tr>
      
      <tr>
        <td>Contact:</td>
        <td colspan="3"><?php echo $student->contact;?></td>
      </tr>
      <tr>
        <td>Email:</td>
        <td colspan="3"><?php echo $student->email;?></td>
      </tr>
      <tr>
        <th colspan="4">Parent Information</th>
        
      </tr>
      <tr>
        <td>Father:</td>
        <td colspan="3"><?php echo $student->father;?></td>
      </tr>
      <tr>
        <td>Mother:</td>
        <td colspan="3"><?php echo $student->mother;?></td>
      </tr>
      
      <tr>
        <td>Contact:</td>
        <td><?php echo $student->parentcontact;?></td>
        <td>Subscribe to SMS?</td>
        <td><?php echo $student->parentsms;?></td>
      </tr>

    </table>
	</div>

</div>

<script src="/js/vendor/jquery.js"></script>
  <script src="/js/foundation.dropdown.js"></script>
  <script src="/js/foundation.min.js"></script>
  <script>
  $(document).foundation();
</script>
</body>
</html>