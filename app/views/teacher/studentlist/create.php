
<!DOCTYPE html>
<html>
<head>
	<title>CSBN PORTAL | Admin</title>
	<link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link rel="stylesheet" href="/css/createstudent.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="/assets/js/vendor/modernizr.js"></script>
	 <link rel="icon" href="http://d15dxvojnvxp1x.cloudfront.net/assets/favicon.ico">
	<link rel="stylesheet" type="text/css" media="all" href="css/styles.css">
	 
</head>
<body>

<div id="header">
  <div class="container">
    <img src="/img/CSBNLogo.png">
      <span id="CSBN">CSBN</span>
      <span id="portal">Portal</span>
      <p id="colegio">Colegio de San Bartolome de Novaliches</p> <?php $users = Session::get('teacher'); $teacher = Teacher::find($users->_id); ?>
          <a href="#" id="admins" data-dropdown="admin" class="tiny button dropdown"><?php echo $teacher->firstname." ".$teacher->lastname?></a><br>
        <ul id="admin" data-dropdown-content class="f-dropdown">
          <li><a href="/teacherchangepass">Change Password</a></li>
          <li><a href="/teacher-logout">Logout</a></li>
        </ul>
    </img>
  </div>
</div>
<div id="line"></div>

<ul class="side-nav">
  <li><a href="/teacher">Student</a></li>
  <li><a href="/teacherlist">Teacher</a>
</ul>
<div id="maincontent">
 <span id="link" style="font-size:8pt">List of Students > Add Student</span>
<?php if(Session::has('message')){?>
	<div class="alert alert-info"><?php Session::get('message');?></div>
<?php }?>
<h3>Add Student</h3>

<!-- if there are creation errors, they will show here -->
<?php if($errors->has()){?>
              <div data-alert class="alert-box alert">
                <?php foreach ($errors->all() as $error) { ?>
                  <li><?php echo $error; ?></li>
                <?php } ?>
              </div>
            <?php } ?> 
            <?php  $message = Session::get('success'); if($message!=null){?>
            <div data-alert class="alert-box success">
                
                  <li><?php echo $message;?></li>
              
              </div>
              <?php }?>


  <form action="/teacher" method="POST" >
    <table class="radius center large-10 medium-8 small-6 large-centered columns">
      
      <tr>
        
        <td><span id="required">*</span>Student Number:</td>
        <td>
          <input type="text" value="" name="studentnumber" id="studentnumber">
        </td>
        <td><span id="required">*</span>Grade/Year Level:</td>
        <td>
          <select name="level" id="level" onchange="" size="1">
           <option value=""></option>
            <option value="Preschool"> Preschool </option>
            <option value="Grade1"> Grade 1 </option>
            <option value="Grade2"> Grade 2 </option>
            <option value="Grade3"> Grade 3 </option>
            <option value="Grade4"> Grade 4</option>
            <option value="Grade5"> Grade 5 </option>
            <option value="Grade6"> Grade 6 </option>
            <option value="1stYear"> 1st Year </option>
            <option value="2ndYear"> 2nd Year </option>
            <option value="3rdYear"> 3rd Year </option>
            <option value="4thYear"> 4th Year </option>
           
          </select>
        </td>
      </tr>
      <tr>
        <td><span id="required">*</span>First Name:</td>
        <td>
          <input type="text" value="" name="firstname" id="firstname">
        </td>
        <td><span id="required">*</span>Last Name:</td>
        <td>
          <input type="text" value="" name="lastname" id="lastname">
        </td>
        
      </tr>
      <tr>
        <td><span id="required">*</span>Gender:</td>
        <td>
          <input type="radio" name="gender" value="Male" id="gender"><label for="gender">Male</label>
          <input type="radio" name="gender" value="Female" id="gender"><label for="gender">Female</label>
        </td>
        <td>Nationality:</td>
        <td>
          <input type="text" value="" name="nationality" id="nationality">
        </td>

      </tr>
      <tr>
        <td><span id="required">*</span>Birthday(mm/dd/yy):</td>
        <td>
          <select name="month" id="month" onchange="" size="1">
            <option value=""></option>
            <option value="01"> January </option>
            <option value="02"> February </option>
            <option value="03"> March </option>
            <option value="04"> April </option>
            <option value="05"> May </option>
            <option value="06"> June </option>
            <option value="07"> July </option>
            <option value="08"> August </option>
            <option value="09"> September </option>
            <option value="10"> October </option>
            <option value="11"> November </option>
            <option value="12"> December </option>
          </select>
          </td>
          <td>

          <select name="day" id="Day" onselect="day" size="1">
            <option value=""></option>
            <option value="01"> 01 </option>
            <option value="02"> 02 </option>
            <option value="03"> 03 </option>
            <option value="04"> 04 </option>
            <option value="05"> 05 </option>
            <option value="06"> 06 </option>
            <option value="07"> 07 </option>
            <option value="08"> 08 </option>
            <option value="09"> 09 </option>
            <option value="10"> 10 </option>
            <option value="11"> 11 </option>
            <option value="12"> 12 </option>
            <option value="13"> 13 </option>
            <option value="14"> 14 </option>
            <option value="15"> 15 </option>
            <option value="16"> 16 </option>
            <option value="17"> 17 </option>
            <option value="18"> 18 </option>
            <option value="19"> 19 </option>
            <option value="20"> 20 </option>
            <option value="21"> 21 </option>
            <option value="22"> 22 </option>
            <option value="23"> 23 </option>
            <option value="24"> 24 </option>
            <option value="25"> 25 </option>
            <option value="26"> 26 </option>
            <option value="27"> 27 </option>
            <option value="28"> 28 </option>
            <option value="29"> 29 </option>
            <option value="30"> 30 </option>
            <option value="31"> 31 </option>
          </select>
          </td>
          <td>
          <select id="year" name="year">
          <option value=""></option>
          <option value="2014">2014</option>
          <option value="2013">2013</option>
          <option value="2012">2012</option>
          <option value="2011">2011</option>
          <option value="2010">2010</option>
          <option value="2009">2009</option>
          <option value="2008">2008</option>
          <option value="2007">2007</option>
          <option value="2006">2006</option>
          <option value="2005">2005</option>
          <option value="2004">2004</option>
          <option value="2003">2003</option>
          <option value="2002">2002</option>
          <option value="2001">2001</option>
          <option value="2000">2000</option>
          <option value="1999">1999</option>
          <option value="1998">1998</option>
          <option value="1997">1997</option>
          <option value="1996">1996</option>
          <option value="1995">1995</option>
          <option value="1994">1994</option>
          <option value="1993">1993</option>
          <option value="1992">1992</option>
          <option value="1991">1991</option>
          <option value="1990">1990</option>
          </select>
        </td>
        
      </tr>
      <tr>
        <td>Address:</td>
        <td colspan="3">
          <input type="text" value="" name="address" id="address">
        </td>
      </tr>
      
      <tr>
        <td>Mobile Number</td>
  
        <td colspan="3">
          <input type="text" value="" placeholder="+63915xxxxxxx"  name="contact" id="contact">
        </td>
      </tr>
      <tr>
        <td>Email:</td>
        <td colspan="3">
          <input type="text" value="" name="email" id="email">
        </td>
      </tr>
      <tr>
        <th colspan="4">Parent Information</th>
      </tr>
      <tr>
        <td>Father:</td>
        <td colspan="3">
          <input type="text" value="" name="father" id="father">
        </td>
      </tr>
      <tr>
        <td>Mother:</td>
        <td colspan="3">
          <input type="text" value="" name="mother" id="mother">
        </td>
      </tr>
      
      <tr>
        <td>Mobile Number:</td>
        <td>

          <input type="text" value="" name="parentcontact" placeholder="+63915xxxxxxx" id="parentcontact">
        </td>
        <td>Subscribe to SMS?</td>
        <td>
          <input type="radio" name="parentsms" value="yes" id="parentsms-yes"><label for="parentsms-yes">Yes</label>
          <input type="radio" name="parentsms" value="no" id="parentsms-no" checked><label for="parentsms-no">No</label>
        </td>
      </tr>
      
      <tr>
        <td colspan="4">
            <input class="button expand" type="submit" value="Create Account" />
        </td>
      </tr>
    </table>
    </form>

</div>
<script src="/js/vendor/jquery.js"></script>
  <script src="/js/foundation.dropdown.js"></script>
  <script src="/js/foundation.min.js"></script>
  <script>
  $(document).foundation();
</script>
</body>
</html>
