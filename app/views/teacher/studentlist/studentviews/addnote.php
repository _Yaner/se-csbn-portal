
<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >
<head>
  <title>CSBN PORTAL | Admin</title>
  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link rel="stylesheet" href="/css/createstudent.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="/assets/js/vendor/modernizr.js"></script>
   <link rel="icon" href="http://d15dxvojnvxp1x.cloudfront.net/assets/favicon.ico">
  <link rel="stylesheet" type="text/css" media="all" href="css/styles.css">
  <style>
  form{
    margin-top:40px;
  }
  </style>



</head>
<body>

<div id="header">
  <div class="container">
      <img src="/img/CSBNLogo.png">
      <span id="CSBN">CSBN</span>
      <span id="portal">Portal</span>
      <p id="colegio">Colegio de San Bartolome de Novaliches</p> <?php $users = Session::get('teacher'); $teacher = Teacher::find($users->_id); ?>
          <a href="#" id="admins" data-dropdown="admin" class="tiny button dropdown"><?php echo $teacher->firstname." ".$teacher->lastname?></a><br>
        <ul id="admin" data-dropdown-content class="f-dropdown">
          <li><a href="/teacherchangepass">Change Password</a></li>
          <li><a href="/teacher-logout">Logout</a></li>
        </ul>
    </img>
  </div>

</div>
<div id="line"></div>

<ul class="side-nav">
  <li><a href="/teacher">Student</a></li>
  <li><a href="/addnote">Add Note</a>
</ul>
<div id="maincontent">

<div id = "TopColor" class = "large-6 large-centered columns"><h4>Add Note</h4></div>

<?php if($errors->has()){?>
              <div data-alert class="alert-box alert">
                <?php foreach ($errors->all() as $error) { ?>
                  <li><?php echo $error; ?></li>
                <?php } ?>
              </div>
            <?php } ?> 
            <?php  $message = Session::get('success'); if($message!=null){?>
            <div data-alert class="alert-box success">
                
                  <li><?php echo $message;?></li>
              
              </div>
              <?php }?>
<form action="/sendnote" method="POST" id="search" style = " margin-top:10px">
	


 <table class="large-6 large-centered columns">
 	<tr>

 		<td>
 	  		<input type="text" name="receiver" placeholder="To: [Student Name]" class = "radius" value="Janine Janer">
		</td>
 	</tr>
 	<tr>
        <td >
        	<input type="text" name="sender" placeholder="From: " value="Admin" class = "radius">
        </td>
    </tr>
    <tr>
        <td colspan="4">
        	<input type="text" placeholder="Title" value="" name="title">
        </td>
    </tr>
    <tr> 
    	<td>
          <textarea row = "3" placeholder="Contents"columns = "5" name="content" style = "height: 150px"></textarea>
        </td>
    </tr>
    <tr>
    	<td>
    		<input id = "send" type="submit" class="tiny button radius right"/>
    	</td>
    </tr>
</div>
</form>



  <script src="/js/vendor/jquery.js"></script>
  <script src="/js/foundation.dropdown.js"></script>
  <script src="/js/foundation.min.js"></script>
  <script>
  $(document).foundation();
</script>


</body>
</html>