
<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >
<head>
  <title>CSBN PORTAL | Admin</title>
  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link rel="stylesheet" href="/css/createstudent.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="/assets/js/vendor/modernizr.js"></script>
   <link rel="icon" href="http://d15dxvojnvxp1x.cloudfront.net/assets/favicon.ico">
  <link rel="stylesheet" type="text/css" media="all" href="css/styles.css">
  <style>
  table{
    margin-top:40px;
  }
  </style>



</head>
<body>

<div id="header">
  <div class="container">
      <img src="/img/CSBNLogo.png">
      <span id="CSBN">CSBN</span>
      <span id="portal">Portal</span>
      <p id="colegio">Colegio de San Bartolome de Novaliches</p> <?php $users = Session::get('teacher'); $teacher = Teacher::find($users->_id); ?>
          <a href="#" id="admins" data-dropdown="admin" class="tiny button dropdown"><?php echo $teacher->firstname." ".$teacher->lastname?></a><br>
        <ul id="admin" data-dropdown-content class="f-dropdown">
          <li><a href="/teacherchangepass">Change Password</a></li>
          <li><a href="/teacher-logout">Logout</a></li>
        </ul>
    </img>
  </div>

</div>
<div id="line"></div>

<ul class="side-nav">
  <li><a href="/teacher">Student</a></li>
  <li><a href="/addnote">Add Note</a>
</ul>
<div id="maincontent">
 

<table class="large-8 medium-6 small-4 large-centered small-centered columns">
    <tr>
      <th>Subject</th>
      <th>Raw Score</th>
    </tr>
    <tr>
      <td>CLE</td>
      <td>
        <div class="progress radius"><span class="meter" style="width:70%"></span></div>
      </td>
    </tr>
    <tr>
      <td>Filipino</td>
      <td>
        <div class="progress radius"><span class="meter" style="width:40%"></span></div>
      </td>
    </tr>
    <tr>
      <td>English</td>
      <td>
        <div class="progress radius"><span class="meter" style="width:50%"></span></div>
      </td>
    </tr>
    <tr>
      <td>Mathematics</td>
      <td>
        <div class="progress radius"><span class="meter" style="width:100%"></span></div>
      </td>
    </tr>
    <tr>
      <td>Science</td>
      <td>
        <div class="progress radius"><span class="meter" style="width:70%"></span></div>
      </td>
    </tr>
    <tr>
      <td>Araling Panlipunan</td>
      <td>
        <div class="progress radius"><span class="meter" style="width:70%"></span></div>
      </td>
    </tr>
    <tr>
      <td>MAPEH</td>
      <td>
        <div class="progress radius"><span class="meter" style="width:70%"></span></div>
      </td>
    </tr>
    <tr>
      <td>TLE/COM</td>
      <td>
        <div class="progress radius"><span class="meter" style="width:70%"></span></div>
      </td>
    </tr>
  </table>
</div>



  <script src="/js/vendor/jquery.js"></script>
  <script src="/js/foundation.dropdown.js"></script>
  <script src="/js/foundation.min.js"></script>
  <script>
  $(document).foundation();
</script>


</body>
</html>