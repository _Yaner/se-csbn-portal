
<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >
<head>
  <title>CSBN PORTAL | Admin</title>
  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link rel="stylesheet" href="/css/createstudent.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="/assets/js/vendor/modernizr.js"></script>
   <link rel="icon" href="http://d15dxvojnvxp1x.cloudfront.net/assets/favicon.ico">
  <link rel="stylesheet" type="text/css" media="all" href="css/styles.css">
  <style>
  table{
    margin-top:40px;
  }
  </style>
  <style>
#ForTop 
{
  background-color: #aeba00;
  height:40px;
  border-radius: 4px;
}
#back{
  background-color: #aeba00;
  padding: 10px;
  padding-top: 12px;
  font-size: 15px;
}
#log
{
  color: white; 
  position: relative; 
  top: 11px;
  font-size: 15px;
  left: -5px;
}

#down{
  background-color: #aeba00;
  padding-bottom: 10px;
  padding-top: 12px;
  margin-top: 1px;
  margin-right: 1px;
}
</style>



</head>
<body>

<div id="header">
  <div class="container">
      <img src="/img/CSBNLogo.png">
      <span id="CSBN">CSBN</span>
      <span id="portal">Portal</span>
      <p id="colegio">Colegio de San Bartolome de Novaliches</p> <?php $users = Session::get('teacher'); $teacher = Teacher::find($users->_id); ?>
          <a href="#" id="admins" data-dropdown="admin" class="tiny button dropdown"><?php echo $teacher->firstname." ".$teacher->lastname?></a><br>
        <ul id="admin" data-dropdown-content class="f-dropdown">
          <li><a href="/teacherchangepass">Change Password</a></li>
          <li><a href="/teacher-logout">Logout</a></li>
        </ul>
    </img>
  </div>

</div>
<div id="line"></div>

<ul class="side-nav">
  <li><a href="/teacher">Student</a></li>
  <li><a href="/addnote">Add Note</a>
</ul>
<div id="maincontent">
<table class="large-10 large-centered columns" style = "border:0; margin-top:40px"> 
  <tr>
    <td colspan = 3>
      <div id = "ForTop"  style = "text-align: center">
        <span style = "float: left">
          <a href="<?php Redirect::back();?>" id="back" class="button radius">< Back</a>
        </span>
        <span id = "log">Logs</span>
          <a href="#" id = "down" data-dropdown="drop" class="small radius button dropdown right">Months</a><br>
            <ul id="drop" data-dropdown-content class="f-dropdown">
              <li><a href="#">May</a></li>
              <li><a href="#">June</a></li>
              <li><a href="#">July</a></li>
            </ul>
      </div>
    </td>
  </tr>
  
  <tr>
    <th>Day</th>
    <th>Date</th>
    <th>Time in</th>
  </tr>
  <?php  foreach($attendances as $attendance){ $date = explode(' ', $attendance->time);?>
  <tr>
    <td><?php echo $date[0];?></td>
    <td><?php echo $date[1]." ".$date[2]." ".$date[3];?></td>
    <td><?php echo $date[4];?></td>
  </tr>
  <?php }?>
</table>
 
</div>



  <script src="/js/vendor/jquery.js"></script>
  <script src="/js/foundation.dropdown.js"></script>
  <script src="/js/foundation.min.js"></script>
  <script>
  $(document).foundation();
</script>


</body>
</html>