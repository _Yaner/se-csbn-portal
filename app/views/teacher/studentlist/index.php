<!DOCTYPE html>
<html>
<head>
	<title>CSBN PORTAL | Admin</title>
	<link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link rel="stylesheet" href="/css/adminstudent.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="/assets/js/vendor/modernizr.js"></script>
	  <link rel="icon" href="http://d15dxvojnvxp1x.cloudfront.net/assets/favicon.ico">
	  <link rel="stylesheet" type="text/css" media="all" href="css/styles.css">
	  
</head>
<body>

<div id="header">
  <div class="container">
    <img src="/img/CSBNLogo.png">
      <span id="CSBN">CSBN</span>
      <span id="portal">Portal</span>
      <p id="colegio">Colegio de San Bartolome de Novaliches</p> <?php $users = Session::get('teacher'); $teacher = Teacher::find($users->_id); ?>
      <?php $users = Session::get('teacher'); $teacher = Teacher::find($users->_id); ?>
          <a href="#" id="admins" data-dropdown="admin" class="tiny button dropdown"><?php echo $teacher->firstname." ".$teacher->lastname?></a><br>
        <ul id="admin" data-dropdown-content class="f-dropdown">
          <li><a href="/teacherchangepass">Change Password</a></li>
          <li><a href="/teacher-logout">Logout</a></li>
        </ul>
    </img>
  </div>
</div>
<div id="line"></div>

<ul class="side-nav">
  <li><a href="/teacher">Student</a></li>
  <li><a href="/addnote">Add Note</a></li>
</ul>
<div id="maincontent">
 <span id="link" style="font-size:8pt">List of Students</span>
 <br>
<?php  $message = Session::get('success'); if($message!=null){?>
    <div data-alert class="alert-box success">
        <li><?php echo $message;?></li>
    </div>
 <?php }?>

<a href="/teacher/create" id="create"><i class="fa fa-plus"></i>Add Student</a>
<table id="keywords" class="table table-striped table-bordered">
	<thead>
		<tr>
			<th><span>Student ID</span></th>
			<th><span>Lastname</span></th>
			<th><span>Firstname</span></th>
			<th><span>Level</span></th>
			
			<th colspan="2">Actions</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($students as $student){?>
		<tr>
			<td><?php echo $student->studentnumber;?></td>
			<td><?php echo $student->lastname;?></td>
			<td><?php echo $student->firstname;?></td>
			<td><?php echo $student->level;?></td>
			<td>
				<a  href="teacher/<?php echo $student->id;?>">Show |</a>
				<a   href="/teacher/<?php echo $student->id;?>/edit"> Edit |</a>
			</td>
			<td>
			<form method="POST" action="teacher/<?php echo $student->id;?>">
					<input type="hidden" value="DELETE" name="_method">
					<input id="delete" type="submit" value="Delete" />
					
				</form>
			</td>
		</tr>
	<?php }?>
	</tbody>
</table>

</div>




<script src="/js/vendor/jquery.js"></script>
  <script src="/js/foundation.dropdown.js"></script>
  <script src="/js/foundation.min.js"></script>
  <script>
  $(document).foundation();
</script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
	  <script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
	  <script type="text/javascript">
		$(function(){
		  $('#keywords').tablesorter(); 
		});
		</script>

</body>
</html>