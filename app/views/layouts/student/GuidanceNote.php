

<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>CSBN Portal</title>
  <link rel="stylesheet" type="text/css" href="/css/style/login.css">
  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="/assets/js/vendor/modernizr.js"></script>
  <style>
  #header{
    padding:0;
    margin:0;
    width:100%;
    height:50px;
    background-color:#819650; 
  }

  #header div img{
    width:40px;
    margin:5px 0 0 10px;
}
#line{
  width:100%;
  height:10px;
  background-color:#f3cf3f;
}
.side-nav{
  width:15%;
  background-color: #363636;
  height:100%;
  float:left;
}
.side-nav li a{
  color:#fff !important;
}
#maincontent{
  float:right;
  width:70%;
  background-color: #fdfef8;


}
#CSBN{
      color:#f6f1a8;
      font-weight: 800;
      font-size:18pt;
      margin-left:10px;
    }
    #portal{
      color:#f39c12;
      font-weight: 800;
      font-size:18pt;
    }
    #colegio{
      color:#d1d1d1;
      font-size: 10pt;
      margin:-20px 0 0 61px;
    }
#user{
  float:right;
}

#TopColor 
{
  margin-top:100px;
    height:40px;
    background-color: #aeba00;
    border-radius: 4px;
}
#OffColor 
{
  background-color: #f5f5f5;
    border-radius: 4px;
    padding-bottom:50px;
    
}
p.offense
{
  padding-top: 20px;
  margin-bottom: 5px;
}
#logout{
  float:right;
  height:20px;
  margin-top:-30px;
  background-color:#ffd519; 
  font-size:10pt;
}

  </style>
</head>
<body>
<div id="header">
  <div class="container">
    <img src="/img/CSBNLogo.png">
      <span id="CSBN">CSBN</span>
      <span id="portal">Portal</span>
      <p id="colegio">Colegio de San Bartolome de Novaliches</p>
      <a href="/student-logout" id="logout">Logout</a>
    </img>
  </div>
</div>
<div id="line"></div>

<ul class="side-nav">
  <li><a href="/profile">Profile</a></li>
  <li><a href="/grades">Grades</a></li>
  <li><a href="/attendance">Attendance</a></li>
  <li><a href="/guidance">Guidance Note</a></li>
</ul>
<div id="maincontent">
  
<div id = "TopColor" class="large-6 large-centered columns"></div>
<div id = "OffColor" class="large-6 large-centered columns">
  <p class= "offense">
    <span style = "color:orange; font-weight: bold"><b>Minor Offense: Cutting Class</b></span>
    <span style = "float: right">21 May 2013</span></p>
  <span style = "font-size: 15px">Lorem ipsum dolor sit amet. Teh qikcu robwn xfo mpsju eovr eht azyl odg. <br><br>
  <span style = "float: right; margin-top: 20px">Posted by: Name of the teacher</span>
</div>
<script src="/js/vendor/jquery.js"></script>
<script src="/js/foundation.min.js"></script>

</div>



  <script src="/js/vendor/jquery.js"></script>
  <script src="/js/foundation.min.js"></script>
</body>
</html>