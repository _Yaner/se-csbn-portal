<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>CSBN Portal</title>
  
  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

  <script src="/js/vendor/modernizr.js"></script>

<script src="/js/vendor/jquery.js"></script>
<script src="/js/foundation/foundation.js"></script>
<script src="/js/foundation/foundation.dropdown.js"></script>



<style>
#ForTop 
{
  background-color: #aeba00;
  height:40px;
  border-radius: 4px;
}
#back{
  background-color: #aeba00;
  padding: 10px;
  padding-top: 12px;
  font-size: 15px;
}
#log
{
  color: white; 
  position: relative; 
  top: 11px;
  font-size: 15px;
  left: -5px;
}

#down{
  background-color: #aeba00;
  padding-bottom: 10px;
  padding-top: 12px;
}
</style>

</head>

<body>
<table class="large-6 large-centered columns" style = "border:0; margin-top:100px"> 
  <tr>
    <td colspan = 3>
      <div id = "ForTop"  style = "text-align: center">
        <span style = "float: left">
          <a href="#" id="back" class="button radius">< Back</a>
        </span>
        <span id = "log">Logs</span>
          <a href="#" id = "down" data-dropdown="drop" class="small radius button dropdown right">Months</a><br>
            <ul id="drop" data-dropdown-content class="f-dropdown">
              <li><a href="#">This is a link</a></li>
              <li><a href="#">This is another</a></li>
              <li><a href="#">Yet another</a></li>
            </ul>
      </div>
    </td>
  </tr>
  
  <tr>
    <th>May 2014</th>
    <th>Day</th>
    <th>Time in</th>
  </tr>
  <tr>
    <td>21 May 2014</td>
    <td>Monday</td>
    <td>6:42am</td>
  </tr>
</table>
<script>
  $(document).foundation();
  
</script>

</body>