<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>CSBN Portal</title>
  <link rel="stylesheet" type="text/css" href="/css/style/login.css">
  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="/assets/js/vendor/modernizr.js"></script>
  <style>
	th {
		background-color: #AEBA00;
	}
  </style>
</head>

<body>
  <div class="gradedetail">
    <table class="small-6 large-6 small-centered large-centered columns">
      <tr>
        <th colspan="3">Subject</th>
      </tr>
      <tr>
        <td>Total Raw Score</td>
        <td><div class="progress radius"><span class="meter" style="width:90%"></span></div></td>
      </tr>
      <tr>
        <td colspan="3">Summary of Grades</td>
      </tr>
      <tr>
        <td>7 Jul 2013</td>
        <td>Seatwork 1</td>
        <td><div class="progress radius"><span class="meter" style="width:90%"></span></div></td>
      </tr>
      <tr>
        <td>21 Jul 2013</td>
        <td>Seatwork 2</td>
        <td><div class="progress radius"><span class="meter" style="width:90%"></span></div></td>
      </tr>
      <tr>
        <td>8 Aug 2013</td>
        <td>Quiz 1</td>
        <td><div class="progress radius"><span class="meter" style="width:90%"></span></div></td>
      </tr>
      <tr>
        <td>20 Aug 2013</td>
        <td>1st Quarter Exam</td>
        <td><div class="progress radius"><span class="meter" style="width:90%"></span></div></td>
      </tr>
    </table>
  </div>
</body>