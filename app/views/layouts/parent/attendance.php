<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>CSBN Portal</title>
  <link rel="stylesheet" type="text/css" href="/css/style/login.css">
  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="/assets/js/vendor/modernizr.js"></script>
  <style>
  #header{
    padding:0;
    margin:0;
    width:100%;
    height:50px;
    background-color:#819650; 
  }

  #header div img{
    width:40px;
    margin:5px 0 0 10px;
}
#line{
  width:100%;
  height:10px;
  background-color:#f3cf3f;
}
.side-nav{
  width:15%;
  background-color: #363636;
  height:100%;
  float:left;
}
.side-nav li a{
  color:#fff !important;
}
#maincontent{
  float:right;
  width:70%;
  background-color: #fdfef8;


}
#CSBN{
      color:#f6f1a8;
      font-weight: 800;
      font-size:18pt;
      margin-left:10px;
    }
    #portal{
      color:#f39c12;
      font-weight: 800;
      font-size:18pt;
    }
    #colegio{
      color:#d1d1d1;
      font-size: 10pt;
      margin:-20px 0 0 61px;
    }
#user{
  float:right;
}

 table,th,td
   {
    border:0px;
   }

   div.header 
   {
    border: 2px solid #6E8B3D;
    background: #6E8B3D;
    width: 750px;
    height: 4%;
    margin-top: 100px;
   }

   div.header2
{
    border: 2px solid #A2CD5A;
    background: #A2CD5A;
    width: 750px;
    height: 2%;
   }

p
{
  margin-right: 26%;
}

  </style>
</head>
<body>
<div id="header">
  <div class="container">
    <img src="/img/CSBNLogo.png">
      <span id="CSBN">CSBN</span>
      <span id="portal">Portal</span>
      <p id="colegio">Colegio de San Bartolome de Novaliches</p>
    </img>
  </div>
</div>
<div id="line"></div>

<ul class="side-nav">
  <li><a href="/profile">Profile</a></li>
  <li><a href="/grades">Grades</a></li>
  <li><a href="/attendance">Attendance</a></li>
  <li><a href="/guidance">Guidance Note</a></li>
</ul>
<div id="maincontent">
  
<center>
    <div class="header"> </div>
  <table>
    <thead1>
      <tr>
        <th width="450">Total: </th>
        <th><font-color="#BCEE68"><font color="#BCEE68">Present:</font></th>
        <th width="10"> </th>
        <th><font color="#FFA500">Late: </font></th>
        <th width="10"> </th>
        <th ><font color="#DC143C">Absent: </font></th>
        <th width="10"> </th>
      </tr>
    </thead1>
  </table></center>
  <center>
    <div class="header2"> </div>
  <table>
    <thead2>
      <tr>
        <th width="200"></th>
        <th>June</th>
        <th>July</th>
        <th>Aug</th>
        <th>Sep</th>
        <th>Oct</th>
        <th>Nov</th>
        <th>Dec</th>
        <th>Jan</th>
        <th>Feb</th>
        <th>Mar</th>
        <th>Apr</th>
      </tr>
    </thead2>
    <tbody>
      <tr>
        <td>School Days</td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
      </tr>
      <tr>
        <td>Days Present</td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
      </tr>
      <tr>
      <td>Days Tardy</td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
      </tr>
    </tbody>
  </table></center>
  <p>
    <a href="#" class="button radius right">View Log</a>
  </p>
</div>



  <script src="/js/vendor/jquery.js"></script>
  <script src="/js/foundation.min.js"></script>
</body>
</html>
