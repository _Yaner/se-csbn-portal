<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>CSBN Portal</title>
  <link rel="stylesheet" type="text/css" href="/css/style/login.css">
  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="/assets/js/vendor/modernizr.js"></script>
  <style>
  #header{
    padding:0;
    margin:0;
    width:100%;
    height:50px;
    background-color:#819650; 
  }

  #header div img{
    width:40px;
    margin:5px 0 0 10px;
}
#line{
  width:100%;
  height:10px;
  background-color:#f3cf3f;
}
.side-nav{
  width:15%;
  background-color: #363636;
  height:100%;
  float:left;
}
.side-nav li a{
  color:#fff !important;
}
#maincontent{
  float:right;
  width:70%;
  background-color: #fdfef8;


}
#CSBN{
      color:#f6f1a8;
      font-weight: 800;
      font-size:18pt;
      margin-left:10px;
    }
    #portal{
      color:#f39c12;
      font-weight: 800;
      font-size:18pt;
    }
    #colegio{
      color:#d1d1d1;
      font-size: 10pt;
      margin:-20px 0 0 61px;
    }
#user{
  float:right;
}

th {
		background-color: #AEBA00;
	}
  </style>
</head>
<body>
<div id="header">
  <div class="container">
    <img src="/img/CSBNLogo.png">
      <span id="CSBN">CSBN</span>
      <span id="portal">Portal</span>
      <p id="colegio">Colegio de San Bartolome de Novaliches</p>
    </img>
  </div>
</div>
<div id="line"></div>

<ul class="side-nav">
  <li><a href="/profile">Profile</a></li>
  <li><a href="/grades">Grades</a></li>
  <li><a href="/attendance">Attendance</a></li>
  <li><a href="/guidance">Guidance Note</a></li>
</ul>
<div id="maincontent">
  
<table class="large-8 medium-6 small-4 large-centered small-centered columns">
		<tr>
			<th>Subject</th>
			<th>Raw Score</th>
		</tr>
		<tr>
			<td>CLE</td>
			<td>
				<div class="progress radius"><span class="meter" style="width:70%"></span></div>
			</td>
		</tr>
		<tr>
			<td>Filipino</td>
			<td>
				<div class="progress radius"><span class="meter" style="width:40%"></span></div>
			</td>
		</tr>
		<tr>
			<td>English</td>
			<td>
				<div class="progress radius"><span class="meter" style="width:50%"></span></div>
			</td>
		</tr>
		<tr>
			<td>Mathematics</td>
			<td>
				<div class="progress radius"><span class="meter" style="width:100%"></span></div>
			</td>
		</tr>
		<tr>
			<td>Science</td>
			<td>
				<div class="progress radius"><span class="meter" style="width:70%"></span></div>
			</td>
		</tr>
		<tr>
			<td>Araling Panlipunan</td>
			<td>
				<div class="progress radius"><span class="meter" style="width:70%"></span></div>
			</td>
		</tr>
		<tr>
			<td>MAPEH</td>
			<td>
				<div class="progress radius"><span class="meter" style="width:70%"></span></div>
			</td>
		</tr>
		<tr>
			<td>TLE/COM</td>
			<td>
				<div class="progress radius"><span class="meter" style="width:70%"></span></div>
			</td>
		</tr>
	</table>

</div>



  <script src="/js/vendor/jquery.js"></script>
  <script src="/js/foundation.min.js"></script>
</body>
</html>
