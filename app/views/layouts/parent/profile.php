<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>CSBN Portal</title>
  <link rel="stylesheet" type="text/css" href="/css/style/login.css">
  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="/assets/js/vendor/modernizr.js"></script>
  <style>
  th {
    background-color: #AEBA00;
  }
  .profile{
    margin-left:-10%;
  }
  
  #image {
    width: 128px;
    height: 128px;
    background-color: #F0F0F0;
  }
  #header{
    padding:0;
    margin:0;
    width:100%;
    height:50px;
    background-color:#819650; 
  }

  #header div img{
    width:40px;
    margin:5px 0 0 10px;
}
#line{
  width:100%;
  height:10px;
  background-color:#f3cf3f;
}
.side-nav{
  width:15%;
  background-color: #363636;
  height:100%;
  float:left;
}
.side-nav li a{
  color:#fff !important;
}
#maincontent{
  float:right;
  width:70%;
  background-color: #fdfef8;


}
#CSBN{
      color:#f6f1a8;
      font-weight: 800;
      font-size:18pt;
      margin-left:10px;
    }
    #portal{
      color:#f39c12;
      font-weight: 800;
      font-size:18pt;
    }
    #colegio{
      color:#d1d1d1;
      font-size: 10pt;
      margin:-20px 0 0 61px;
    }
#user{
  float:right;
}
  </style>
</head>
<body>
<div id="header">
  <div class="container">
    <img src="/img/CSBNLogo.png">
      <span id="CSBN">CSBN</span>
      <span id="portal">Portal</span>
      <p id="colegio">Colegio de San Bartolome de Novaliches</p>
    </img>
  </div>
</div>
<div id="line"></div>

<ul class="side-nav">
  <li><a href="/parent">Profile</a></li>
  <li><a href="/parent-grades">Grades</a></li>
  <li><a href="/parent-attendance">Attendance</a></li>
  <li><a href="/parent-guidance">Guidance Note</a></li>
</ul>
<div id="maincontent">
  
<?php
  $name = 'Juan Chuu';
  $gender = 'Male';
  $birthday = 'July 28, 1995';
  $nationality = 'Filipino';
  $address = '123 Street, Lorem Ipsum dolor sit amet PH';
  $contact_number = '+639111123456';
  $email = 'JuanChuu@gmail.com';
?>


<div class="profile">
    <table class="radius center large-10 medium-8 small-6 large-centered columns">
      <tr>
        <td colspan="4">
          <div id="image" class="left"></div>
          <div class="left">
            <span id="fullname"><?php echo $name; ?></span><br />
            <span id="studentid">2010222121</span><br />
            <span id="section">4th year - Prudence</span>
          </div>
        </td>
      </tr>
      <tr>
        <th colspan="4"></th>
      </tr>
      <tr>
        <td>Name:</td>
        <td><?php echo $name; ?></td>
        <td>Gender:</td>
        <td><?php echo $gender; ?></td>
      </tr>
      <tr>
        <td>Birthday:</td>
        <td><?php echo $birthday; ?></td>
        <td>Nationality:</td>
        <td><?php echo $nationality; ?></td>
      </tr>
      <tr>
        <td>Address:</td>
        <td colspan="3"><?php echo $address; ?></td>
      </tr>
      <tr>
        <th colspan="3">Contact Information</th>
        <th>Edit</th>
      </tr>
      <tr>
        <td>Contact:</td>
        <td colspan="3"><?php echo $contact_number; ?></td>
      </tr>
      <tr>
        <td>Email:</td>
        <td colspan="3"><?php echo $email; ?></td>
      </tr>
      <tr>
        <th colspan="3">Parent Information</th>
        <th>Edit</th>
      </tr>
      <tr>
        <td>Father:</td>
        <td colspan="3">Mr. Yo Chuu</td>
      </tr>
      <tr>
        <td>Mother:</td>
        <td colspan="3">Mrs. Mitchel Chuu</td>
      </tr>
      <tr>
        <td>Address:</td>
        <td colspan="3">123 Street, Lorem Ipsum dolor sit amet PH</td>
      </tr>
      <tr>
        <td>Contact:</td>
        <td>+639123456789</td>
        <td>Subscribe to SMS?</td>
        <td>Yes</td>
      </tr>
      <tr>
        <th colspan="3">Guardian Information</th>
        <th>Edit</th>
      </tr>
      <tr>
        <td>Guardian:</td>
        <td colspan="3">Carmina Sudden Mercy</td>
      </tr>
      <tr>
        <td>Address:</td>
        <td colspan="3">456 Street, Lorem Ipsum dolor sit amet PH</td>
      </tr>
      <tr>
        <td>Contact:</td>
        <td>+639123456789</td>
        <td>Subscribe to SMS?</td>
        <td>No</td>
      </tr>
    </table>
  </div>

</div>



  <script src="/js/vendor/jquery.js"></script>
  <script src="/js/foundation.min.js"></script>
</body>
</html>