<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>CSBN Portal</title>
  <link rel="stylesheet" type="text/css" href="/css/style/login.css">
  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="/assets/js/vendor/modernizr.js"></script>

<style>
#TopColor	
{
	margin-top: 100px;
    height:40px;
    background-color: #aeba00;
    border-radius: 4px;
    padding-top: 12px;
    text-align: center;
    color: white;
}

#send{
	background-color: #aeba00;
	height: 10px;
	padding-top: 8px;
	padding-bottom: 20px;
	border-radius: 7px;
}

#search {

}

#search input[type="text"] {
    background: url(/img/search-white.png) no-repeat 140px 12px #444;
    border: 0 none;
 }

#search input[type="text"]:focus {
    background: url(/img/search-dark.png) no-repeat 140px 12px #fcfcfc;
}

</style>
</head>

<body>
<div class = "large-7 large-centered columns">
	<form id="search" style = "float: right; margin-top:40px">
    	<input type="text" placeholder="Search Name" class = "radius">
	</form>
	</div>

<br>
<div id = "TopColor" class = "large-6 large-centered columns">Add Note</div>
 <table class="large-6 large-centered columns">
    <tr>
        <td colspan="4">
        	<input type="text" value="Title">
        </td>
    </tr>
    <tr>
    	<td>
          <textarea row = "3" columns = "5" style = "height: 150px">Contents: Lorem upsum Dolor Sit amet</textarea>
        </td>
    </tr>
    <tr>
    	<td>
    		<a href="#" id = "send" class="tiny button radius right">Send Note</a>
    	</td>
    </tr>
   <script src="/js/vendor/jquery.js"></script>
<script src="/js/foundation.min.js"></script>
</body>