<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>CSBN Portal</title>
  <link rel="stylesheet" type="text/css" href="/css/style/login.css">
  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="/assets/js/vendor/modernizr.js"></script>

<style>
#ForTop 
{
  background-color: #aeba00;
  height:10px;
  border-radius: 4px;
}
#back{
  background-color: #aeba00;
  padding: 10px;
  padding-top: 12px;
  font-size: 15px;
}

#update{
  color: #009933;
  font-size: 10px;
  float:right;
}

#subjects
{
  color: white; 
  position: relative; 
  top: 11px;
  font-size: 15px;
  left: -25px;
}
#send{
  background-color: #aeba00;
  height: 10px;
  padding-top: 8px;
  padding-bottom: 20px;
  border-radius: 7px;
}

#search {

}

#search input[type="text"] {
    background: url(/img/search-white.png) no-repeat 140px 12px #444;
    border: 0 none;
 }

#search input[type="text"]:focus {
    background: url(/img/search-dark.png) no-repeat 140px 12px #fcfcfc;
}

</style>
</head>

<body>
<div class = "large-7 large-centered columns">
  <form id="search" style = "float: right; margin-top:40px">
      <input type="text" placeholder="Search Name" class = "radius">
  </form>
  </div>

<br>

<table class="large-6 large-centered columns" style = "border:0; margin-top:100px"> 
  <tr>
    <td colspan = 2 id = "ForTop"  style = "text-align: center">
        <span style = "float: left">
          <a href="#" id="back" class="button radius">< Back</a>
        </span>
        <span id = "subjects">Subjects</span>
    </td>
  </tr>
  <tr>
    <td><center>CLE</td></center>
    <td id = "update">Last Updated: 03/03/14 06:30pm</td>
  </tr>
  <tr>
    <td><center>Music</td></center>
    <td id = "update">Last Updated: 03/03/14 06:30pm</td>
  </tr>
</table>     

   <script src="/js/vendor/jquery.js"></script>
<script src="/js/foundation.min.js"></script>
</body>