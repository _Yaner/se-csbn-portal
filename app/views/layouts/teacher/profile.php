<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>CSBN Portal</title>
  <link rel="stylesheet" type="text/css" href="/css/style/login.css">
  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="/assets/js/vendor/modernizr.js"></script>
  <style>
  #content{
    margin-top: 10px;
  }

  th {
    background-color: #AEBA00;
  }
  #image {
    width: 128px;
    height: 128px;
    background-color: #F0F0F0;
  }

#search {

}

#search input[type="text"] {
    background: url(/img/search-white.png) no-repeat 120px 6px #444;
    border: 0 none;
    font: bold 12px Arial,Helvetica,Sans-serif;
    color: #d7d7d7;
    width:150px;
    padding: 6px 15px 6px 35px;
    -webkit-border-radius: 20px;
    -moz-border-radius: 20px;
    border-radius: 20px;
    text-shadow: 0 2px 2px rgba(0, 0, 0, 0.3); 
    -webkit-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1), 0 1px 3px rgba(0, 0, 0, 0.2) inset;
    -moz-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1), 0 1px 3px rgba(0, 0, 0, 0.2) inset;
    box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1), 0 1px 3px rgba(0, 0, 0, 0.2) inset;
    -webkit-transition: all 0.7s ease 0s;
    -moz-transition: all 0.7s ease 0s;
    -o-transition: all 0.7s ease 0s;
    transition: all 0.7s ease 0s;
    }

#search input[type="text"]:focus {
    background: url(/img/search-dark.png) no-repeat 120px 6px #fcfcfc;
    color: #6a6f75;
    width: 200px;
    -webkit-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1), 0 1px 0 rgba(0, 0, 0, 0.9) inset;
    -moz-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1), 0 1px 0 rgba(0, 0, 0, 0.9) inset;
    box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1), 0 1px 0 rgba(0, 0, 0, 0.9) inset;
    text-shadow: 0 2px 3px rgba(0, 0, 0, 0.1);
    }

  </style>
</head>

<body>
  <div class="profile">
  <form method="get" action="/search" id="search" style="margin-left:60%; margin-right:30%">
    <input name="q" type="text" size="5" placeholder="Search..." />
  </form><br>
    <table id="content" class="radius center large-6 large-centered columns">
      <tr>
        <td colspan="4">
          <div id="image" class="left"></div>
          <div class="left">
            <span id="fullname">Juan Chii</span><br />
            <span id="studentid">2010222121</span><br />
            <span id="section">4th year - Prudence</span>

            <div class="button-bar">
              <u class="button-group [radius round]">
                <li><a href="#" class="[tiny small large] button [alert success secondary] [disabled]">View Grades</a></li>
                <li><a href="#" class="[tiny small large] button [alert success secondary] [disabled]">View Attendance</a></li>
                <li><a href="#" class="[tiny small large] button [alert success secondary] [disabled]">Add Notes</a></li>
                <li><a href="#" class="[tiny small large] button [alert success secondary] [disabled]">Change Password</a></li>
              </u>
            </div>
         </div>   
        </div>
        </td>
        
      </tr>
      <tr>
        <th colspan="4"></th>
      </tr>
      <tr>
        <td>Name:</td>
        <td>Juan Chuu</td>
        <td>Gender:</td>
        <td>Male</td>
      </tr>
      <tr>
        <td>Birthday:</td>
        <td>July 28, 1995</td>
        <td>Nationality:</td>
        <td>Filipino</td>
      </tr>
      <tr>
        <td>Address:</td>
        <td colspan="3">123 Street, Lorem Ipsum dolor sit amet PH</td>
      </tr>
      <tr>
        <th colspan="3">Contact Information</th>
        <th>Edit</th>
      </tr>
      <tr>
        <td>Contact:</td>
        <td colspan="3">-</td>
      </tr>
      <tr>
        <td>Email:</td>
        <td colspan="3">JuanChuu@gmail.com</td>
      </tr>
      <tr>
        <th colspan="3">Parent Information</th>
        <th>Edit</th>
      </tr>
      <tr>
        <td>Father:</td>
        <td colspan="3">Mr. Yo Chuu</td>
      </tr>
      <tr>
        <td>Mother:</td>
        <td colspan="3">Mrs. Mitchel Chuu</td>
      </tr>
      <tr>
        <td>Address:</td>
        <td colspan="3">123 Street, Lorem Ipsum dolor sit amet PH</td>
      </tr>
      <tr>
        <td>Contact:</td>
        <td>+639123456789</td>
        <td>Subscribe to SMS?</td>
        <td>Yes</td>
      </tr>
      <tr>
        <th colspan="3">Guardian Information</th>
        <th>Edit</th>
      </tr>
      <tr>
        <td>Guardian:</td>
        <td colspan="3">Carmina Sudden Mercy</td>
      </tr>
      <tr>
        <td>Address:</td>
        <td colspan="3">456 Street, Lorem Ipsum dolor sit amet PH</td>
      </tr>
      <tr>
        <td>Contact:</td>
        <td>+639123456789</td>
        <td>Subscribe to SMS?</td>
        <td>No</td>
      </tr>
    </table>
  </div>
</body>
