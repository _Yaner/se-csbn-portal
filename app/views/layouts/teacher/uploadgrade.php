<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>CSBN Portal</title>
  <link rel="stylesheet" type="text/css" href="/css/style/login.css">
  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="/assets/js/vendor/modernizr.js"></script>

<style>
#ForTop 
{
  background-color: #aeba00;
  height:10px;
  border-radius: 4px;
}
#back{
  background-color: #aeba00;
  color: white;
  font-size: 12px;
}

#back:hover{
  color: #6B8E23 !important;
}

#update{
  color: #009933;
}

#subjects
{
  color: white; 
  position: relative; 
  font-size: 15px;
  left: -25px;
}
#upload{
  background-color: #008B8B;
  height: 10px;
  border-radius: 7px;
  margin-top: 15px;
  font-size: 12px;
  padding: 3px;
  color: white;
}

#upload:hover{
  background-color: #5F9F9F !important;
}

#search input[type="text"] {
    background: url(/img/search-white.png) no-repeat 140px 12px #444;
    border: 0 none;
 }

#search input[type="text"]:focus {
    background: url(/img/search-dark.png) no-repeat 140px 12px #fcfcfc;
}

</style>
</head>

<body>
<div class = "large-7 large-centered columns">
  <form id="search" style = "float: right; margin-top:40px">
      <input type="text" placeholder="Search Name" class = "radius">
  </form>   
  </div>

<br>

<table class="large-6 large-centered columns" style = "border:0; margin-top:100px"> 
  <tr>
    <td colspan = 3 id = "ForTop"  style = "text-align: center">
        <span style = "float: left">
          <a href="#" id="back">< Back</a>
        </span>
        <span id = "subjects">Temperance</span>
    </td>
  </tr>
  <tr>
     <td id = "update" colspan = 2>Last Updated: 03/03/14 06:30pm</td>
     <td colspan = 2>
        <a href="#" id = "upload" >Upload Grade</a>
      </td>
  </tr> 
  <tr height = "30px">
    <td colspan = 3></td>
  </tr>
  <tr>
    <td colspan = 3>
      <a href="#">Surname, Firstname</a>
    </td>
  </tr>
  <tr>
    <td colspan = 3>
      <a href="#">Surname, Firstname</a>
    </td>
  </tr>
  <tr>
    <td colspan = 3>
      <a href="#">Surname, Firstname</a>
    </td>
  </tr>
</table>     

   <script src="/js/vendor/jquery.js"></script>
<script src="/js/foundation.min.js"></script>
</body>