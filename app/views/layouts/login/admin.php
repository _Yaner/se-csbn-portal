<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>CSBN Portal</title>
  <link rel="stylesheet" type="text/css" href="/css/style/login.css">
  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="/assets/js/vendor/modernizr.js"></script>
  <style>
    body{
      background-color:#eee;
    }
    label{
      color:#fff;
      font-weight:700;
      font-size:11pt;
    }

    #main h3{
        color:#fff;
        font-size:21pt;
        font-weight: 800;
    }
    #main p{
        color:#d7d7d7;
        font-size:9pt;
        font-weight: 600;
    }

    #CSBN{
      color:#f6f1a8;
      font-weight: 800;
      font-size:18pt;
    }
    #portal{
      color:#f39c12;
      font-weight: 800;
      font-size:18pt;
    }
    #logo{
      width:50px;
      margin-right: 15px;
      
    }
    #colegio{
      color:#959595;
      font-size: 7pt;
    }
    #headerline{
      width:100%;
      height:40px;
      background-color: #ffd519;
    }
    #footerline{
      width:100%;
      height:40px;
      background-color: #707070;
      position: absolute;
      bottom:0;

    }
    #title{
      padding:0;
      margin: 0;
      color:#959595;
      font-size:8pt;

    }
    .signin{
      border:3px solid #819650;
    }

  </style>
</head>
<body>
<div id="headerline"></div>
<div style="height:50px;"></div>
 <center><img id="logo" src="/img/CSBNLogo.png" class="small-2" style=""/>
 </center>
 <center id="title"><span id="CSBN">CSBN</span><span id="portal">Portal</span><br/>Colegio de San Bartlome de Novaliches</center>
  <div id="main" class="clearfix">

      <div class="signin medium-4 columns small-centered panel callout radius clearfix" >
      
      <h3>Admin Login</h3>
     
                   <?php if($errors->has()){?>
              <div data-alert class="alert-box alert">
                <?php foreach ($errors->all() as $error) { ?>
                  <li><?php echo $error; ?></li>
                <?php } ?>
              </div>
            <?php } ?> 
          <form action="/admin/login" method="POST">
              <div class="row">
                  <label>Username</label>
                  <input type="text" name="username" placeholder="Email">
              </div>
              <div class="row">
                  <label>Password</label>
                  <input type="password" name="password" placeholder="Password">
              </div>
              <input class="medium-12 columns button right" type="submit" value="Login" style="background-color:#ffd519; font-size:12pt; font-weight: 700;"/>
          </form>
      </div>
  </div>
  <div id="footerline"></div>
  <script src="/js/vendor/jquery.js"></script>
  <script src="/js/foundation.min.js"></script>
</body>
</html>