
<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>CSBN Portal</title>
  <link rel="stylesheet" type="text/css" href="/css/style/login.css">
  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="/assets/js/vendor/modernizr.js"></script>
  <style>
  #header{
    padding:0;
    margin:0;
    width:100%;
    height:50px;
    background-color:#819650; 
  }

  #header div img{
    width:40px;
    margin:5px 0 0 10px;
}
#line{
  width:100%;
  height:10px;
  background-color:#f3cf3f;
}
.side-nav{
  width:15%;
  background-color: #363636;
  height:100%;
  float:left;
}
.side-nav li a{
  color:#fff !important;

}
#maincontent{
  float:right;
  width:70%;
  background-color: #fdfef8;


}
#CSBN{
      color:#f6f1a8;
      font-weight: 800;
      font-size:18pt;
      margin-left:10px;
    }
    #portal{
      color:#f39c12;
      font-weight: 800;
      font-size:18pt;
    }
    #colegio{
      color:#d1d1d1;
      font-size: 10pt;
      margin:-20px 0 0 61px;
    }
#user{
  float:right;
}
th {
    background-color: #AEBA00;
  }
  #image {
    width: 128px;
    height: 128px;
    background-color: #F0F0F0;
  }
  #logout{
  float:right;
  height:20px;
  margin-top:-30px;
  background-color:#ffd519; 
  font-size:10pt;
}
#required{
  font-size:15px;
  color:red !important;
}
tr,td,input{ margin:0!important;}

  </style>
</head>
<body>
<div id="header">
  <div class="container">
    <img src="/img/CSBNLogo.png">
      <span id="CSBN">CSBN</span>
      <span id="portal">Portal</span>
      <p id="colegio">Colegio de San Bartolome de Novaliches</p>
      <a href="/admin-logout" id="logout">Logout</a>
    </img>
  </div>
</div>
<div id="line"></div>

<ul class="side-nav">
  <li><a href="/students/list">Student</a></li>
  <li><a href="/teacher">Teacher</a></li>
  
</ul>
<div id="maincontent">
   <?php if($errors->has()){?>
              <div data-alert class="alert-box alert">
                <?php foreach ($errors->all() as $error) { ?>
                  <li><?php echo $error; ?></li>
                <?php } ?>
              </div>
            <?php } ?> 
            <?php  $message = Session::get('success'); if($message!=null){?>
            <div data-alert class="alert-box success">
                
                  <li><?php echo $message;?></li>
              
              </div>
              <?php }?>

  <div class="profile">
  <form action="/admin/addStudent" method="POST">
    <table class="radius center large-10 medium-8 small-6 large-centered columns">
      
      <tr>
        
        <td><span id="required">*</span>Student Number:</td>
        <td>
          <input type="text" value="" name="studentnumber" id="studentnumber">
        </td>
        <td><span id="required">*</span>Grade/Year Lever:</td>
        <td>
          <select name="level" id="level" onchange="" size="1">
            <option value="Preschool"> Preschool </option>
            <option value="Grade 1"> Grade 1 </option>
            <option value="Grade 2"> Grade 2 </option>
            <option value="Grade 3"> Grade 3 </option>
            <option value="Grade 4"> Grade 4</option>
            <option value="Grade 5"> Grade 5 </option>
            <option value="Grade 6"> Grade 6 </option>
            <option value="1st Year"> 1st Year </option>
            <option value="2nd Year"> 2nd Year </option>
            <option value="3rd Year"> 3rd Year </option>
            <option value="4th Year"> 4th Year </option>
           
          </select>
        </td>
      </tr>
      <tr>
        <td><span id="required">*</span>First Name:</td>
        <td>
          <input type="text" value="" name="firstname" id="firstname">
        </td>
        <td><span id="required">*</span>Last Name:</td>
        <td>
          <input type="text" value="" name="lastname" id="lastname">
        </td>
        
      </tr>
      <tr>
        <td><span id="required">*</span>Gender:</td>
        <td>
          <input type="radio" name="gender" value="Male" id="gender"><label for="gender">Male</label>
          <input type="radio" name="gender" value="Female" id="gender"><label for="gender">Female</label>
        </td>
        <td>Nationality:</td>
        <td>
          <input type="text" value="" name="nationality" id="nationality">
        </td>

      </tr>
      <tr>
        <td><span id="required">*</span>Birthday(mm/dd/yy):</td>
        <td>
          <select name="month" id="month" onchange="" size="1">
            <option value="01"> January </option>
            <option value="02"> February </option>
            <option value="03"> March </option>
            <option value="04"> April </option>
            <option value="05"> May </option>
            <option value="06"> June </option>
            <option value="07"> July </option>
            <option value="08"> August </option>
            <option value="09"> September </option>
            <option value="10"> October </option>
            <option value="11"> November </option>
            <option value="12"> December </option>
          </select>
          </td>
          <td>

          <select name="day" id="Day" onselect="day" size="1">
            <option value="01"> 01 </option>
            <option value="02"> 02 </option>
            <option value="03"> 03 </option>
            <option value="04"> 04 </option>
            <option value="05"> 05 </option>
            <option value="06"> 06 </option>
            <option value="07"> 07 </option>
            <option value="08"> 08 </option>
            <option value="09"> 09 </option>
            <option value="10"> 10 </option>
            <option value="11"> 11 </option>
            <option value="12"> 12 </option>
            <option value="13"> 13 </option>
            <option value="14"> 14 </option>
            <option value="15"> 15 </option>
            <option value="16"> 16 </option>
            <option value="17"> 17 </option>
            <option value="18"> 18 </option>
            <option value="19"> 19 </option>
            <option value="20"> 20 </option>
            <option value="21"> 21 </option>
            <option value="22"> 22 </option>
            <option value="23"> 23 </option>
            <option value="24"> 24 </option>
            <option value="25"> 25 </option>
            <option value="26"> 26 </option>
            <option value="27"> 27 </option>
            <option value="28"> 28 </option>
            <option value="29"> 29 </option>
            <option value="30"> 30 </option>
            <option value="31"> 31 </option>
          </select>
          </td>
          <td>
          <select id="year" name="year">
          <option value="2014">2014</option>
          <option value="2013">2013</option>
          <option value="2012">2012</option>
          <option value="2011">2011</option>
          <option value="2010">2010</option>
          <option value="2009">2009</option>
          <option value="2008">2008</option>
          <option value="2007">2007</option>
          <option value="2006">2006</option>
          <option value="2005">2005</option>
          <option value="2004">2004</option>
          <option value="2003">2003</option>
          <option value="2002">2002</option>
          <option value="2001">2001</option>
          <option value="2000">2000</option>
          <option value="1999">1999</option>
          <option value="1998">1998</option>
          <option value="1997">1997</option>
          <option value="1996">1996</option>
          <option value="1995">1995</option>
          <option value="1994">1994</option>
          <option value="1993">1993</option>
          <option value="1992">1992</option>
          <option value="1991">1991</option>
          <option value="1990">1990</option>
          </select>
        </td>
        
      </tr>
      <tr>
        <td>Address:</td>
        <td colspan="3">
          <input type="text" value="" name="address" id="address">
        </td>
      </tr>
      
      <tr>
        <td>Contact:</td>
        <td colspan="3">
          <input type="text" value="" name="contact" id="contact">
        </td>
      </tr>
      <tr>
        <td>Email:</td>
        <td colspan="3">
          <input type="text" value="" name="email" id="email">
        </td>
      </tr>
      <tr>
        <th colspan="4">Parent Information</th>
      </tr>
      <tr>
        <td>Father:</td>
        <td colspan="3">
          <input type="text" value="" name="father" id="father">
        </td>
      </tr>
      <tr>
        <td>Mother:</td>
        <td colspan="3">
          <input type="text" value="" name="mother" id="mother">
        </td>
      </tr>
      
      <tr>
        <td>Contact:</td>
        <td>
          <input type="text" value="" name="parentcontact" id="parentcontact">
        </td>
        <td>Subscribe to SMS?</td>
        <td>
          <input type="radio" name="parentsms" value="yes" id="parentsms-yes"><label for="parentsms-yes">Yes</label>
          <input type="radio" name="parentsms" value="no" id="parentsms-no"><label for="parentsms-no">No</label>
        </td>
      </tr>
      
      <tr>
        <td colspan="4">
            <input class="button expand" type="submit" value="Create Account" />
        </td>
      </tr>
    </table>
    </form>
  </div>
  

</div>



  <script src="/js/vendor/jquery.js"></script>
  <script src="/js/foundation.dropdown.js"></script>
  <script src="/js/foundation.min.js"></script>
  <script>
  $(document).foundation();
</script>
</body>
</html>