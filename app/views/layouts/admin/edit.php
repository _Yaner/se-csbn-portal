<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>CSBN Portal</title>
  <link rel="stylesheet" type="text/css" href="/css/style/login.css">
  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="/assets/js/vendor/modernizr.js"></script>
  <style>
  #header{
    padding:0;
    margin:0;
    width:100%;
    height:50px;
    background-color:#819650; 
  }

  #header div img{
    width:40px;
    margin:5px 0 0 10px;
}
#line{
  width:100%;
  height:10px;
  background-color:#f3cf3f;
}
.side-nav{
  width:15%;
  background-color: #363636;
  height:100%;
  float:left;
}
.side-nav li a{
  color:#fff !important;
}
#maincontent{
  float:right;
  width:70%;
  background-color: #fdfef8;


}
#CSBN{
      color:#f6f1a8;
      font-weight: 800;
      font-size:18pt;
      margin-left:10px;
    }
    #portal{
      color:#f39c12;
      font-weight: 800;
      font-size:18pt;
    }
    #colegio{
      color:#d1d1d1;
      font-size: 10pt;
      margin:-20px 0 0 61px;
    }
#user{
  float:right;
}

#ForTop 
{
  background-color: #aeba00;
  height:10px;
  border-radius: 4px;
}
#back{
  background-color: #aeba00;
  padding: 10px;
  padding-top: 12px;
  font-size: 15px;
}

#update{
  color: #009933;
  font-size: 10px;
  float:right;
}

#subjects
{
  color: white; 
  position: relative; 
  top: 11px;
  font-size: 15px;
  left: -25px;
}
#send{
  background-color: #aeba00;
  height: 10px;
  padding-top: 8px;
  padding-bottom: 20px;
  border-radius: 7px;
}

#search {

}

#search input[type="text"] {
    background: url(/img/search-white.png) no-repeat 140px 12px #444;
    border: 0 none;
 }

#search input[type="text"]:focus {
    background: url(/img/search-dark.png) no-repeat 140px 12px #fcfcfc;
}


.dropdown{
  float:right;
  margin-top:-33px;
  margin-right:20px;
  background-color: #819650;
  -moz-box-shadow:    1px 1px 1px 1px #757859;
  -webkit-box-shadow: 1px 1px 1px 1px #757859;
  box-shadow:         1px 1px 1px 1px #757859;
}

.f-dropdown{
  max-width: 110px;
 
}

.f-dropdown li a{
  font-size:11px !important;
}
  </style>
</head>
<body>
<div id="header">
  <div class="container">
    <img src="/img/CSBNLogo.png">
      <span id="CSBN">CSBN</span>
      <span id="portal">Portal</span>
      <p id="colegio">Colegio de San Bartolome de Novaliches
</p>
<a href="#" data-dropdown="drop1" class="tiny button dropdown">Admin</a><br>
        <ul id="drop1" data-dropdown-content class="f-dropdown">
          
          <li><a href="/changepass">Change Password</a></li>
          <li><a href="/student-logout">Logout</a></li>
        </ul>
    </img>
  </div>
</div>
<div id="line"></div>

<ul class="side-nav">
  <li><a href="/students">Student</a></li>
  <li><a href="/teacher">Teacher</a></li>
</ul>


<div id="maincontent">
  
<div class="activity-service">
  <h1>Edit Student</h1>
<form class="form-horizontal" role="form" action="/signup" method="POST">
   <div class="form-group">
     <label for="name" class="col-sm-2 control-label">Name</label>
     <div class="col-sm-10">
       <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $user->name; ?>">
     </div>
   </div>
  <div class="form-group">
    <label for="mobile" class="col-sm-2 control-label">Mobile</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="mobile" id="mobile" placeholder="09xxxxxxxxx" maxlength="11" value="<?php echo $user->mobile; ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="email" class="col-sm-2 control-label">Email</label>
    <div class="col-sm-10">
      <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $user->email; ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="password" class="col-sm-2 control-label">Password</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" name="password" id="password" placeholder="Password">
    </div>
  </div>
  <div class="form-group">
    <label for="confirm" class="col-sm-2 control-label">Confirm Password</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" name="password_confirmation" id="confirm" placeholder="Confirm Password">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <input type="submit" class="btn btn-success" value="UPDATE">
      <a href="/admin/users" class="btn btn-success">CANCEL</a>
    </div>
  </div>
</form>
  </table>
</div>
</div>



  <script src="/js/vendor/jquery.js"></script>
  <script src="/js/foundation.dropdown.js"></script>
  <script src="/js/foundation.min.js"></script>
  <script>
  $(document).foundation();
</script>
</body>
</html>

