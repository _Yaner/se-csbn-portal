<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>CSBN Portal</title>
  <link rel="stylesheet" type="text/css" href="/css/style/login.css">
  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="/assets/js/vendor/modernizr.js"></script>
  <style>
  th {
    background-color: #AEBA00;
  }
  .profile{
    margin-left:-10%;
  }
  
  #image {
    width: 128px;
    height: 128px;
    background-color: #F0F0F0;
  }
  #header{
    padding:0;
    margin:0;
    width:100%;
    height:50px;
    background-color:#819650; 
  }

  #header div img{
    width:40px;
    margin:5px 0 0 10px;
}
#line{
  width:100%;
  height:10px;
  background-color:#f3cf3f;
}
.side-nav{
  width:15%;
  background-color: #363636;
  height:100%;
  float:left;
}
.side-nav li a{
  color:#fff !important;
}
#maincontent{
  float:right;
  width:70%;
  background-color: #fdfef8;


}
#CSBN{
      color:#f6f1a8;
      font-weight: 800;
      font-size:18pt;
      margin-left:10px;
    }
    #portal{
      color:#f39c12;
      font-weight: 800;
      font-size:18pt;
    }
    #colegio{
      color:#d1d1d1;
      font-size: 10pt;
      margin:-20px 0 0 61px;
    }
#user{
  float:right;
}
#logout{
  float:right;
  height:20px;
  margin-top:-30px;
  background-color:#ffd519; 
  font-size:10pt;
}

.dropdown{
  float:right;
  margin-top:-33px;
  margin-right:10px;
  background-color: #819650;
  -moz-box-shadow:    1px 1px 1px 1px #757859;
  -webkit-box-shadow: 1px 1px 1px 1px #757859;
  box-shadow:         1px 1px 1px 1px #757859;
}

.f-dropdown{
  max-width: 124px;
 
}

.f-dropdown li a{
  font-size:12px !important;
}
  </style>





</head>
<body>
<?php $users = Session::get('admin'); foreach ($students as $student){ if($student->id!=$users->profile_id){ ?>
<div id="header">
  <div class="container">
    <img src="/img/CSBNLogo.png">
      <span id="CSBN">CSBN</span>
      <span id="portal">Portal</span>
      <p id="colegio">Colegio de San Bartolome de Novaliches</p>
       
        
        <a href="#" data-dropdown="drop1" class="tiny button dropdown"><?php echo $student->firstname; echo " ".$student->lastname;?></a><br>
        <ul id="drop1" data-dropdown-content class="f-dropdown">
          <li><a href="/editcontact">Edit Contact</a></li>
          <li><a href="/changepass">Change Password</a></li>
          <li><a href="/student-logout">Logout</a></li>
        </ul>
    </img>
  </div>

</div>
<div id="line"></div>

<ul class="side-nav">
  <li><a href="/profile">Profile</a></li>
  <li><a href="/grades">Grades</a></li>
  <li><a href="/attendance">Attendance</a></li>
  <li><a href="guidance">Guidance Note</a></li>
</ul>
<div id="maincontent">

<div class="profile">
    <table class="radius center large-10 medium-8 small-6 large-centered columns">
      <tr>
        <td colspan="4">
          <div id="image" class="left"></div>
          <div class="left">
            <span id="fullname"><?php echo $student->firstname; echo $student->lastname;?></span><br />
            <span id="studentid"><?php echo $student->studentnumber;?></span><br />
            <span id="section">4th year - Prudence</span>
          </div>
        </td>
      </tr>
      <tr>
        <th colspan="4"></th>
      </tr>
      <tr>
        <td>Name:</td>
        <td><?php echo $student->firstname; echo " ".$student->lastname;?></td>
        <td>Gender:</td>
        <td><?php echo $student->gender;?></td>
      </tr>
      <tr>
        <td>Birthday:</td>
        <td><?php echo $student->birthday;?></td>
        <td>Nationality:</td>
        <td><?php echo $student->nationality;?></td>
      </tr>
      <tr>
        <td>Address:</td>
        <td colspan="3"><?php echo $student->address;?></td>
      </tr>
      
      <tr>
        <td>Contact:</td>
        <td colspan="3"><?php echo $student->contact;?></td>
      </tr>
      <tr>
        <td>Email:</td>
        <td colspan="3"><?php echo $student->email;?></td>
      </tr>
      <tr>
        <th colspan="4">Parent Information</th>
        
      </tr>
      <tr>
        <td>Father:</td>
        <td colspan="3"><?php echo $student->father;?></td>
      </tr>
      <tr>
        <td>Mother:</td>
        <td colspan="3"><?php echo $student->mother;?></td>
      </tr>
      
      <tr>
        <td>Contact:</td>
        <td><?php echo $student->parentcontact;?></td>
        <td>Subscribe to SMS?</td>
        <td><?php echo $student->parentsms;?></td>
      </tr>

    </table>
  </div>

</div>



  <script src="/js/vendor/jquery.js"></script>
  <script src="/js/foundation.dropdown.js"></script>
  <script src="/js/foundation.min.js"></script>
  <script>
  $(document).foundation();
</script>

<?php
  }}
 ?>
</body>
</html>