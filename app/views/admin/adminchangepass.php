
<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >
<head>
  <title>CSBN PORTAL | Admin</title>
  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link rel="stylesheet" href="/css/createstudent.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="/assets/js/vendor/modernizr.js"></script>
   <link rel="icon" href="http://d15dxvojnvxp1x.cloudfront.net/assets/favicon.ico">
  <link rel="stylesheet" type="text/css" media="all" href="css/styles.css">
  <style>
  form{
    margin-top:40px;
  }
  </style>



</head>
<body>

<div id="header">
  <div class="container">
      <img src="/img/CSBNLogo.png">
      <span id="CSBN">CSBN</span>
      <span id="portal">Portal</span>
      <p id="colegio">Colegio de San Bartolome de Novaliches</p>
          <a href="#" id="admins" data-dropdown="admin" class="tiny button dropdown"><?php echo "Admin"?></a><br>
        <ul id="admin" data-dropdown-content class="f-dropdown">
          <li><a href="/adminchangepass">Change Password</a></li>
          <li><a href="/admin-logout">Logout</a></li>
        </ul>
    </img>
  </div>

</div>
<div id="line"></div>

<ul class="side-nav">
  <li><a href="/studentlist">Student</a></li>
  <li><a href="/teacherlist">Teacher</a>
</ul>
<div id="maincontent">
   <?php if($errors->has()){?>
              <div data-alert class="alert-box alert">
                <?php foreach ($errors->all() as $error) { ?>
                  <li><?php echo $error; ?></li>
                <?php } ?>
              </div>
            <?php } ?> 
            <?php  $message = Session::get('message'); if($message!=null){?>
            <div data-alert class="alert-box success">
                
                  <li><?php echo $message;?></li>
              
              </div>
              <?php }?>
            
              
    <form action="/adminchangepass" method="POST" class="large-3 large-centered columns">
      <p>Old Password: <br><input class = "radius" type="password" name="Old-password" value="<?php echo Input::old('Old-password'); ?>"></p>
      <br>
   
       <p><label for="New-password"> New Password: <input type="password" id="New-password" name="New-password"></label></p>
       <p><div class="" id="passwordStrength"></div></p> 

       <p><label for="Confirm-password">Confirm New Password: <input type="password" id="Confirm-password" name="Confirm-password"></label></p>
       <p><div class="" id="match"></div></p>
      <br>
      <input  type="submit"id="save" class="button radius right" id="Confirm-password"/>
      <p><div class="" id="match"></div></p>
    </form>


</div>


  <script>

  </script>
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
  <script src="/js/vendor/jquery.js"></script>
  <script src="/js/foundation.dropdown.js"></script>
  <script src="/js/foundation.min.js"></script>
  <script>
  $(document).foundation();
  $(document).ready(function() {
 
    $('#New-password, #Confirm-password').on('keyup', function(e) {
 
        if($('#New-password').val() == '' && $('#Confirm-password').val() == '')
        {
            $('#passwordStrength').removeClass().html('');
 
            return false;
        }else if($('#New-password').val() != '' && $('#Confirm-password').val() == '')
        {
            $('#match').removeClass().html('');
        }
 
     else if($('#New-password').val() != '' && $('#Confirm-password').val() != '' && $('#New-password').val() != $('#Confirm-password').val())
      {
        $('#match').removeClass().addClass('alert alert-error').html('Passwords do not match!');
        $('#match').css("color","#FF3333");
          return false;
      }
      else {
        $('#match').removeClass().addClass('alert alert-error').html('Passwords match!');
        $('#match').css("color","green");  
      }
 
        // Must have capital letter, numbers and lowercase letters
        var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
 
        // Must have either capitals and lowercase letters or lowercase and numbers
        var mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
 
        // Must be at least 6 characters long
        var okRegex = new RegExp("(?=.{6,}).*", "g");
 
        if (okRegex.test($(this).val()) === false) {
            // If ok regex doesn't match the password
          $('#passwordStrength').removeClass().addClass('alert alert-error').html('Password must be 6 characters long.');
          $('#passwordStrength').css("color","#888888");  

        } else if (strongRegex.test($(this).val())) {
            // If reg ex matches strong password
            $('#passwordStrength').removeClass().addClass('alert alert-success').html('Strong!');
            $('#passwordStrength').css("color","green");  

        } else if (mediumRegex.test($(this).val())) {
            // If medium password matches the reg ex
            $('#passwordStrength').removeClass().addClass('alert alert-info').html('Medium (Try using capital letters, more numbers and special characters)');
            $('#passwordStrength').css("color","#FF75193");  

        } else {
            // If password is ok
            $('#passwordStrength').removeClass().addClass('alert alert-error').html('Weak! (Try using numbers and capital letters)');
              $('#passwordStrength').css("color","#FF3333");  
        }
        
        return true;
    });
});
</script>


</body>
</html>