
<!DOCTYPE html>
<html>
<head>
  <title>CSBN PORTAL | Admin</title>
  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link rel="stylesheet" href="/css/createstudent.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="/assets/js/vendor/modernizr.js"></script>
   <link rel="icon" href="http://d15dxvojnvxp1x.cloudfront.net/assets/favicon.ico">
  <link rel="stylesheet" type="text/css" media="all" href="css/styles.css">
   
</head>
<body>

<div id="header">
  <div class="container">
    <img src="/img/CSBNLogo.png">
      <span id="CSBN">CSBN</span>
      <span id="portal">Portal</span>
      <p id="colegio">Colegio de San Bartolome de Novaliches</p>

       <a href="#" id="admins" data-dropdown="admin" class="tiny button dropdown"><?php echo "Admin"?></a><br>
        <ul id="admin" data-dropdown-content class="f-dropdown">
          <li><a href="/adminchangepass">Change Password</a></li>
          <li><a href="/admin-logout">Logout</a></li>
        </ul>
    </img>
  </div>
</div>
<div id="line"></div>

<ul class="side-nav">
  <li><a href="/studentlist">Student</a></li>
  <li><a href="/teacherlist">teacher</a>
</ul>
<div id="maincontent">
 <a class="button tiny secondary" type="button" href="/teacherlist"> < Back</a>
 <br>
<?php if(Session::has('message')){?>
  <div class="alert alert-info"><?php Session::get('message');?></div>
<?php }?>

<!-- if there are creation errors, they will show here -->
<?php if($errors->has()){?>
              <div data-alert class="alert-box alert">
                <?php foreach ($errors->all() as $error) { ?>
                  <li><?php echo $error; ?></li>
                <?php } ?>
              </div>
            <?php } ?> 
            <?php  $message = Session::get('success'); if($message!=null){?>
            <div data-alert class="alert-box success">
                
                  <li><?php echo $message;?></li>
              
              </div>
              <?php }?>


  
<form action="/teacherlist/<?php echo $teacher->id; ?>" method="POST" enctype="multipart/form-data">
<input type="hidden" name="_method" value="PUT">
     <table class="radius center large-10 medium-8 small-6 large-centered columns">
      
      <tr>
        
        <td><span id="required">*</span>Username:</td>
        <td>
          <input type="text" value="<?php echo $teacher->username; ?>" name="username" id="username">
        </td>
        <td>Contact Number</td>
        <td><input type="text" value="<?php echo $teacher->contact; ?>" name="contact"></td>
      </tr>
      <tr>
        <td><span id="required">*</span>First Name:</td>
        <td>
          <input type="text" value="<?php echo $teacher->firstname; ?>" name="firstname" id="firstname" >
        </td>
        <td><span id="required">*</span>Last Name:</td>
        <td>
          <input type="text" value="<?php echo $teacher->lastname; ?>" name="lastname" id="lastname">
        </td>
        
      </tr>

      <tr>
        <td>Position:</td>
        <td><input type="text" value="<?php echo Input::old('position'); ?>" name="position" id="position"></td>
         <td><span id="required">*</span>Advisory:</td>
        <td>
          <select name="advisory" id="level" onchange="" size="1">
           <option value=""></option>
            <option value="Preschool"> Preschool </option>
            <option value="Grade1"> Grade 1 </option>
            <option value="Grade2"> Grade 2 </option>
            <option value="Grade3"> Grade 3 </option>
            <option value="Grade4"> Grade 4</option>
            <option value="Grade5"> Grade 5 </option>
            <option value="Grade6"> Grade 6 </option>
            <option value="1stYear"> 1st Year </option>
            <option value="2ndYear"> 2nd Year </option>
            <option value="3rdYear"> 3rd Year </option>
            <option value="4thYear"> 4th Year </option>
           
          </select>
        </td>
        <td></td>
      </tr>
       <tr>
       <td><span id="required">*</span>Birthday(mm/dd/yy):</td>
      <td><input colspan="3"type="text" id="datepicker" value="<?php echo Input::old('birthday'); ?>" name="birthday"></td>
      <td><span id="required">*</span>Subject Major</td>
        <td>
          <select name="subjectmajor" id="level" onchange="" size="1">
           <option value=""></option>
            <option value="English">English</option>
            <option value="Filipino">Filipino</option>
            <option value="PE "> PE </option>
            <option value="Math"> Math</option>
            <option value="Math">Science</option>
          </select>
        </td>
      </tr>
     
      
      <tr>
        <td colspan="4">
            <input class="button expand" type="submit" value="Save" />
        </td>
      </tr>
    </table>
    </form>

</div>

<script src="/js/vendor/jquery.js"></script>
  <script src="/js/foundation.dropdown.js"></script>
  <script src="/js/foundation.min.js"></script>
  <script>
  $(document).foundation();
  
  $( document ).ready(function() {
    
    var optionValue = $('#month').val();
    $("#mon").val(optionValue).find("option[value=" + optionValue +"]").attr('selected', true);

    var optionValue1 = $('#dayy').val();
    $("#day").val(optionValue1).find("option[value=" + optionValue1 +"]").attr('selected', true);

    var optionValue2 = $('#yearr').val();
    $("#year").val(optionValue2).find("option[value=" + optionValue2 +"]").attr('selected', true);
  });
</script>
</body>
</html>
