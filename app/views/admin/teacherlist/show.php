
<!DOCTYPE html>
<html>
<head>
	<title>CSBN PORTAL | Admin</title>
	<link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link rel="stylesheet" href="/css/createstudent.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="/assets/js/vendor/modernizr.js"></script>
	 <link rel="icon" href="http://d15dxvojnvxp1x.cloudfront.net/assets/favicon.ico">
	<link rel="stylesheet" type="text/css" media="all" href="css/styles.css">
	 <style>
      td
      {
        text-align: left;
        padding:5px !important;
      }

      #fullname
      {
        margin-right:100px;
      }

    

   </style>
</head>
<body>

<div id="header">
  <div class="container">
    <img src="/img/CSBNLogo.png">
      <span id="CSBN">CSBN</span>
      <span id="portal">Portal</span>
      <p id="colegio">Colegio de San Bartolome de Novaliches</p>
          <a href="#" id="admins" data-dropdown="admin" class="tiny button dropdown"><?php echo "Admin"?></a><br>
        <ul id="admin" data-dropdown-content class="f-dropdown">
          <li><a href="/adminchangepass">Reset Password</a></li>
          <li><a href="/admin-logout">Logout</a></li>
        </ul>
    </img>
  </div>
</div>
<div id="line"></div>

<ul class="side-nav">
  <li><a href="/studentlist">Student</a></li>
  <li><a href="/teacherlist">teacher</a>
</ul>

<div id="maincontent">
<ul class="breadcrumbs bcindex" id="bc" style="width:70%;margin:0 0 0 20%;">
 <li><a href="/teacherlist">Teacher List  &nbsp;&nbsp;> </a></li>
 <li>Teacher Profile  &nbsp;&nbsp; </li>
</ul>
 <a class="button tiny secondary" type="button" href="/teacherlist"> < Back</a>
 <br>
<?php if(Session::has('message')){?>
	<div class="alert alert-info"><?php Session::get('message');?></div>
<?php }?>

<!-- if there are creation errors, they will show here -->
<?php if($errors->has()){?>
              <div data-alert class="alert-box alert">
                <?php foreach ($errors->all() as $error) { ?>
                  <li><?php echo $error; ?></li>
                <?php } ?>
              </div>
            <?php } ?> 
            <?php  $message = Session::get('success'); if($message!=null){?>
            <div data-alert class="alert-box success">
                
                  <li><?php echo $message;?></li>
              
              </div>
              <?php }?>


     <table class="radius large-10 medium-8 small-6 large-centered columns table-striped table-bordered">
      <tr>
        <td colspan="4">
          <div id="image" class="left"></div>
          <div class="left">
            <span id="fullname"><?php echo $teacher->firstname; echo $teacher->lastname;?></span><br />
            <span id="studentid"><?php echo $teacher->username;?></span><br />
            <span id="section">Teacher</span>
          </div>

            <div class="button-bar">
              <ul class="button-group [radius round]">
                <li><a  href="/teacherlist/<?php echo $teacher->id;?>/edit" class="[tiny small large] button [alert success secondary] [disabled]">Edit Profile</a></li>
                <li><a href="/studentlist" class="[tiny small large] button [alert success secondary] [disabled]">Students</a></li>
                <li><a href="/teacherlist/<?php echo $teacher->id;?>/reset" class="[tiny small large] button [alert success secondary] [disabled]">Reset Password</a></li>
                 <li><a href="/attendance" class="[tiny small large] button alert [disabled]">Delete</a></li>
              </ul>
            </div>
        </td>

      </tr>
      <tr>
        <th colspan="4">Basic Information</th>
      </tr>
      <tr>
        
        <td>Username:</td>
        <td>
          <?php echo $teacher->username;?>
        </td>
        <td>Position:</td>
        <td> <?php echo $teacher->position;?></td>
       
      </tr>
      <tr>
        <td>First Name:</td>
        <td>
          <?php echo $teacher->firstname;?>
        </td>
        <td>Last Name:</td>
        <td>
          <?php echo $teacher->lastname;?>
        </td>
        
      </tr>
       <tr>
        <td>Birthday(mm/dd/yy):</td>
        <td><?php echo $teacher->birthday;?></td>
        <td>Contact:</td>
        <td><?php echo $teacher->contact;?></td>
      </tr>
      <tr>
        <th colspan="4"></th>
        
      </tr>

       <tr>
        <td>Adivisory Class:</td>
        <td><?php echo $teacher->advisory;?></td>
        <td>Subject Major:</td>
        <td><?php echo $teacher->subjectmajor?></td>
      </tr>
      <tr>
        <th colspan="4"></th>
        
      </tr>
      </tr>
      <tr>
        <td>Updated At:</td>
        <td>
          <?php echo $teacher->updated_at;?>
        </td>
        <td>Created At:</td>
        <td>
          <?php echo $teacher->created_at;?>
        </td>
        
      </tr>
      

    </table>
	 


</div>

<script src="/js/vendor/jquery.js"></script>
  <script src="/js/foundation.dropdown.js"></script>
  <script src="/js/foundation.min.js"></script>
  <script>
  $(document).foundation();
</script>
</body>
</html>