
<!DOCTYPE html>
<html>
<head>
	<title>CSBN PORTAL | Admin</title>
	  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
  <link rel="stylesheet" href="/resources/demos/style.css">

  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link rel="stylesheet" href="/css/createstudent.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="/assets/js/vendor/modernizr.js"></script>
   <link rel="icon" href="http://d15dxvojnvxp1x.cloudfront.net/assets/favicon.ico">
  <link rel="stylesheet" type="text/css" media="all" href="css/styles.css">


	 
</head>
<body>

<div id="header">
  <div class="container">
    <img src="/img/CSBNLogo.png">
      <span id="CSBN">CSBN</span>
      <span id="portal">Portal</span>
      <p id="colegio">Colegio de San Bartolome de Novaliches</p>
          <a href="#" id="admins" data-dropdown="admin" class="tiny button dropdown"><?php echo "Admin"?></a><br>
        <ul id="admin" data-dropdown-content class="f-dropdown">
          <li><a href="/adminchangepass">Change Password</a></li>
          <li><a href="/admin-logout">Logout</a></li>
        </ul>
    </img>
  </div>
</div>
<div id="line"></div>

<ul class="side-nav">
  <li><a href="/studentlist">Student</a></li>
  <li><a href="/teacherlist">teacher</a>
</ul>
<div id="maincontent">
 <a href="/teacherlist">List of Students > </a><a href="/student/create">Add Student</a>
 <br>
<?php if(Session::has('message')){?>
	<div class="alert alert-info"><?php Session::get('message');?></div>
<?php }?>
<h3>Add Teacher</h3>

<!-- if there are creation errors, they will show here -->
<?php if($errors->has()){?>
              <div data-alert class="alert-box alert">
                <?php foreach ($errors->all() as $error) { ?>
                  <li><?php echo $error; ?></li>
                <?php } ?>
              </div>
            <?php } ?> 
            <?php  $message = Session::get('success'); if($message!=null){?>
            <div data-alert class="alert-box success">
                
                  <li><?php echo $message;?></li>
              
              </div>
              <?php }?>


  <form action="/teacherlist" method="POST" >
    <table class="radius center large-10 medium-8 small-6 large-centered columns">
      
      <tr>
        
        <td><span id="required">*</span>Username:</td>
        <td>
          <input type="text" value="<?php echo Input::old('username'); ?>" name="username" id="username">
        </td>
       <td>Contact Number</td>
        <td><input type="text" value="<?php echo Input::old('contact'); ?>" name="contact"></td>
       
      </tr>
      <tr>
        <td><span id="required">*</span>First Name:</td>
        <td>
          <input type="text" value="<?php echo Input::old('firstname'); ?>" name="firstname" id="firstname" >
        </td>
        <td><span id="required">*</span>Last Name:</td>
        <td>
          <input type="text" value="<?php echo Input::old('lastname'); ?>" name="lastname" id="lastname">
        </td>
        
      </tr>
      <tr>
       <td>Position:</td>
        <td><input type="text" value="<?php echo Input::old('position'); ?>" name="position" id="position"></td>
         <td><span id="required">*</span>Advisory:</td>
        <td>
          <select name="advisory" id="level" onchange="" size="1">
           <option value=""></option>
            <option value="Preschool"> Preschool </option>
            <option value="Grade1"> Grade 1 </option>
            <option value="Grade2"> Grade 2 </option>
            <option value="Grade3"> Grade 3 </option>
            <option value="Grade4"> Grade 4</option>
            <option value="Grade5"> Grade 5 </option>
            <option value="Grade6"> Grade 6 </option>
            <option value="1stYear"> 1st Year </option>
            <option value="2ndYear"> 2nd Year </option>
            <option value="3rdYear"> 3rd Year </option>
            <option value="4thYear"> 4th Year </option>
           
          </select>
        </td>
        <td></td>
       
       </tr>
       <tr>    
       <td><span id="required">*</span>Birthday(mm/dd/yy):</td>
      <td><input colspan="3"type="text" id="datepicker" name="birthday"></td>
      <td><span id="required">*</span>Subject Major</td>
        <td>
          <select name="subjectmajor" id="level" onchange="" size="1">
           <option value=""></option>
            <option value="English">English</option>
            <option value="Filipino">Filipino</option>
            <option value="PE "> PE </option>
            <option value="Math"> Math</option>
            <option value="Math">Science</option>
          </select>
        </td>
     
       <!-- <td>
          <select name="month" id="month" onchange="" size="1">
            <option value="01"> January </option>
            <option value="02"> February </option>
            <option value="03"> March </option>
            <option value="04"> April </option>
            <option value="05"> May </option>
            <option value="06"> June </option>
            <option value="07"> July </option>
            <option value="08"> August </option>
            <option value="09"> September </option>
            <option value="10"> October </option>
            <option value="11"> November </option>
            <option value="12"> December </option>
          </select>
          </td>
          <td>

          <select name="day" id="Day" onselect="day" size="1">
            <option value="01"> 01 </option>
            <option value="02"> 02 </option>
            <option value="03"> 03 </option>
            <option value="04"> 04 </option>
            <option value="05"> 05 </option>
            <option value="06"> 06 </option>
            <option value="07"> 07 </option>
            <option value="08"> 08 </option>
            <option value="09"> 09 </option>
            <option value="10"> 10 </option>
            <option value="11"> 11 </option>
            <option value="12"> 12 </option>
            <option value="13"> 13 </option>
            <option value="14"> 14 </option>
            <option value="15"> 15 </option>
            <option value="16"> 16 </option>
            <option value="17"> 17 </option>
            <option value="18"> 18 </option>
            <option value="19"> 19 </option>
            <option value="20"> 20 </option>
            <option value="21"> 21 </option>
            <option value="22"> 22 </option>
            <option value="23"> 23 </option>
            <option value="24"> 24 </option>
            <option value="25"> 25 </option>
            <option value="26"> 26 </option>
            <option value="27"> 27 </option>
            <option value="28"> 28 </option>
            <option value="29"> 29 </option>
            <option value="30"> 30 </option>
            <option value="31"> 31 </option>
          </select>
          </td>
          <td>
          <select id="year" name="year">
            <?php $year=2014; for($i=0; $i<60;$i++ ){?>
             <option value="<?php echo $year-=1;?>"><?php echo $year;?></option>
            <?php }?>
          </select>
        </td>-->
        
      </tr>
     
      
      <tr>
        <td colspan="4">
            <input class="button expand" type="submit" value="Create Account" />
        </td>
      </tr>
    </table>
    </form>

</div>

  <script src="/js/foundation.dropdown.js"></script>
  <script src="/js/foundation.min.js"></script>
     
    <script>
  $(document).foundation();
   $( "#datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: '1944:2014'
    });
</script>
</body>
</html>
