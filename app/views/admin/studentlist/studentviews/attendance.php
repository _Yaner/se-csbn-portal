
<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >
<head>
  <title>CSBN PORTAL | Admin</title>
  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link rel="stylesheet" href="/css/createstudent.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="/assets/js/vendor/modernizr.js"></script>
   <link rel="icon" href="http://d15dxvojnvxp1x.cloudfront.net/assets/favicon.ico">
  <link rel="stylesheet" type="text/css" media="all" href="css/styles.css">
  <style>
  center{
    margin-top:40px;
  }
  </style>



</head>
<body>

<div id="header">
  <div class="container">
      <img src="/img/CSBNLogo.png">
      <span id="CSBN">CSBN</span>
      <span id="portal">Portal</span>
      <p id="colegio">Colegio de San Bartolome de Novaliches</p>
          <a href="#" id="admins" data-dropdown="admin" class="tiny button dropdown"><?php echo "Admin"?></a><br>
        <ul id="admin" data-dropdown-content class="f-dropdown">
          <li><a href="/adminchangepass">Change Password</a></li>
          <li><a href="/admin-logout">Logout</a></li>
        </ul>
    </img>
  </div>

</div>
<div id="line"></div>

<ul class="side-nav">
  <li><a href="/studentlist">Student</a></li>
  <li><a href="/teacherlist">Teacher</a>
</ul>
<div id="maincontent">
    
<center>
    <div class="header"> </div>
  <table>
    <thead1>
      <tr>
        <th width="450">Total: </th>
        <th><font-color="#BCEE68"><font color="#BCEE68">Present:</font></th>
        <th width="10"> </th>
        <th><font color="#FFA500">Late: </font></th>
        <th width="10"> </th>
        <th ><font color="#DC143C">Absent: </font></th>
        <th width="10"> </th>
      </tr>
    </thead1>
  </table></center>
  <center>
    <div class="header2"> </div>
  <table>
    <thead2>
      <tr>
        <th width="200"></th>
        <th>June</th>
        <th>July</th>
        <th>Aug</th>
        <th>Sep</th>
        <th>Oct</th>
        <th>Nov</th>
        <th>Dec</th>
        <th>Jan</th>
        <th>Feb</th>
        <th>Mar</th>
        <th>Apr</th>
      </tr>
    </thead2>
    <tbody>
      <tr>
        <td>School Days</td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
      </tr>
      <tr>
        <td>Days Present</td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
      </tr>
      <tr>
      <td>Days Tardy</td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
      </tr>
    </tbody>
  </table></center>
  <p>
    <a href="/studentlist/attendance/viewlogs" class="button radius right">View Log</a>
  </p>
</div>



  <script src="/js/vendor/jquery.js"></script>
  <script src="/js/foundation.dropdown.js"></script>
  <script src="/js/foundation.min.js"></script>
  <script>
  $(document).foundation();
</script>


</body>
</html>