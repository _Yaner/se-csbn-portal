<!DOCTYPE html>
<html>
<head>
	<title>CSBN PORTAL | Admin</title>
	 

	<link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link rel="stylesheet" href="/css/adminstudent.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="/assets/js/vendor/modernizr.js"></script>
	  <link rel="icon" href="http://d15dxvojnvxp1x.cloudfront.net/assets/favicon.ico">
	  <link rel="stylesheet" type="text/css" media="all" href="css/styles.css">
	   <link rel="stylesheet" type="text/css" media="all" href="assets/jquery-ui.css">
	   
</head>
<body>

<div id="header">
  <div class="container">
    <img src="/img/CSBNLogo.png">
      <span id="CSBN">CSBN</span>
      <span id="portal">Portal</span>
      <p id="colegio">Colegio de San Bartolome de Novaliches</p>
          <a href="#" id="admins" data-dropdown="admin" class="tiny button dropdown"><?php echo "Admin"?></a><br>
        <ul id="admin" data-dropdown-content class="f-dropdown">
          <li><a href="/adminchangepass">Change Password</a></li>
          <li><a href="/admin-logout">Logout</a></li>
        </ul>
    </img>
  </div>
</div>

<div id="line"></div>

<ul class="side-nav">
  <li><a href="/studentlist">Student</a></li>
  <li><a href="/teacherlist">Teacher</a>
</ul>
<ul class="breadcrumbs bcindex" id="bc" style="width:70%;margin:0 0 0 20%;">
 <li><a href="/studentlist">Main  &nbsp;&nbsp;> </a></li>
</ul>
   <form action="/search" method="POST" >
 <div class="ui-widget" id="test" id="searchbar">
  <label for="tags">Search Student: </label>
  <input id="tags" name="term">
  <input type="submit" value="search" id="save" class="button radius right search"/>
</div>
</form>

<div id="maincontent" class="breadcrumbs">

 <span id="link" style="font-size:8pt">SEARCH RESULT</span>
 <br>
<?php  $message = Session::get('success'); if($message!=null){?>
    <div data-alert class="alert-box success">
        <li><?php echo $message;?></li>
    </div>
 <?php }?>
<br>

<table id="keywords" class="table table-striped table-bordered">
	<thead>
		<tr>
			<th><span>Student ID</span></th>
			<th><span>Lastname</span></th>
			<th><span>Firstname</span></th>
			<th><span>Level</span></th>
			
			<th colspan="2">Actions</th>
		</tr>
	</thead>
	<tbody>
	
		<tr>
			<td><?php echo $result->studentnumber;?></td>
			<td><?php echo $result->lastname;?></td>
			<td><?php echo $result->firstname;?></td>
			<td><?php echo $result->level;?></td>
			<td>
				<a  href="studentlist/<?php echo $result->id;?>">Show |</a>
				<a   href="/studentlist/<?php echo $result->id;?>/edit"> Edit |</a>
			</td>
			<td>
			<form method="POST" action="studentlist/<?php echo $result->id;?>">
					<input type="hidden" value="DELETE" name="_method">
					<input id="delete" type="submit" value="Delete" />
					
				</form>
			</td>
		</tr>

	
	</tbody>
</table>

</div>




<script src="/js/vendor/jquery.js"></script>
  <script src="/js/foundation.dropdown.js"></script>
  <script src="/js/foundation.min.js"></script>
  <script>
  $(document).foundation();
</script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
	  <script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
	  <script src="assets/jquery-ui.js"></script>
	  <script type="text/javascript">
		$(function(){
		  $('#keywords').tablesorter(); 
		});
		</script>
		<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.0.4/angular.min.js"></script>
	  <script>
	   $(document).ready(function(){
	

	  });


	  	$('#test').append("<?php $array =""; foreach($students as $s){$array.=$s->lastname.', '.$s->firstname.':'; } ?>");
	  
	  	var str = "<?php echo $array;?>";
	  	console.log("h>"+str);
	  	var result = str.split(":");
	  	console.log(result);
	  $(function() {
    $( "#tags" ).autocomplete({
      source: result
    });
  });
	  </script>

</body>
</html>