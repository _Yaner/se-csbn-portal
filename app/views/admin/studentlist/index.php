<!DOCTYPE html>
<html>
<head>
	<title>CSBN PORTAL | Admin</title>
	 

	<link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link rel="stylesheet" href="/css/adminstudent.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="/assets/js/vendor/modernizr.js"></script>
  <link rel="icon" href="http://d15dxvojnvxp1x.cloudfront.net/assets/favicon.ico">
 <link rel="stylesheet" type="text/css" media="all" href="css/styles.css">
 <link rel="stylesheet" type="text/css" media="all" href="assets/jquery-ui.css">

	   
</head>
<body>

<div id="header">
  <div class="container">
    <img src="/img/CSBNLogo.png">
      <span id="CSBN">CSBN</span>
      <span id="portal">Portal</span>
      <p id="colegio">Colegio de San Bartolome de Novaliches</p>
          <a href="#" id="admins" data-dropdown="admin" class="tiny button dropdown"><?php echo "Admin"?></a><br>
        <ul id="admin" data-dropdown-content class="f-dropdown">
          <li><a href="/adminchangepass">Change Password</a></li>
          <li><a href="/admin-logout">Logout</a></li>
        </ul>
    </img>
  </div>
</div>

<div id="line"></div>

<ul class="side-nav">
  <li><a href="/studentlist">Student</a></li>
  <li><a href="/teacherlist">Teacher</a>
</ul>
<ul class="breadcrumbs bcindex" id="bc" style="width:70%;margin:0 0 0 20%;">
 <li><a href="/studentlist">Main  &nbsp;&nbsp;> </a></li>
</ul>
	<!--<div ng-cloak id="searchbar" ng-app="app" ng-controller="MainController">
		<input ng-cloak ng-model ="myfilter.name" value="search" id="tags">
             {{myfilter.str | date}}
        <div ng-repeat="user in mydata.arr | filter:myfilter">
                {{user}}
        </div>
     </div>-->
  <form action="/search" method="POST" >
 <div class="ui-widget" id="test" id="searchbar">
  <label for="tags">Search Student: </label>
  <input id="tags" name="term">
  <input type="submit" value="search" id="save" class="button radius right search"/>
</div>
</form>

<div id="maincontent" class="breadcrumbs">

 <span id="link" style="font-size:8pt">List of Students</span>
 <br>
<?php  $message = Session::get('success'); if($message!=null){?>
    <div data-alert class="alert-box success">
        <li><?php echo $message;?></li>
    </div>
 <?php }?>
<br>

<a href="/studentlist/create" id="create"><i class="fa fa-plus"></i>Add Student</a>
<table id="keywords" class="table table-striped table-bordered">
	<thead>
		<tr>
			<th><span>Student ID</span></th>
			<th><span>Lastname</span></th>
			<th><span>Firstname</span></th>
		
			
			<th colspan="3">Actions</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($students as $student):?>
		<tr>
			<td><?php echo $student->studentnumber;?></td>
			<td><?php echo $student->lastname;?></td>
			<td><?php echo $student->firstname;?></td>

			<td colspan="2">
				<a  href="studentlist/<?php echo $student->id;?>">Show |</a>
				<a   href="/studentlist/<?php echo $student->id;?>/edit"> Edit |</a>
			</td>
			<td>
			<form method="POST" action="studentlist/<?php echo $student->id;?>">
					<input type="hidden" value="DELETE" name="_method">
					<input id="delete" type="submit" value="Delete" />
					
				</form>
			</td>
		</tr>
	<?php endforeach;?>
	<?php echo $students->links();?>
	</tbody>
</table>

</div>




<script src="/js/vendor/jquery.js"></script>
  <script src="/js/foundation.dropdown.js"></script>
  <script src="/js/foundation.min.js"></script>
  <script>
  $(document).foundation();
</script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
	  <script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
	  <script src="assets/jquery-ui.js"></script>
	  <script type="text/javascript">
		$(function(){
		  $('#keywords').tablesorter(); 
		});
		</script>
		<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.0.4/angular.min.js"></script>
	  <script>
	  var app = angular.module('app',[]);
	  app.controller('MainController', function($scope){
	
	  	console.log("<?php $counter=0; foreach($students as $student){ echo $student->lastname; $counter++; }?>");
	  	
		var counter = "<?php echo $counter;?>";
		console.log(">"+counter);


	$scope.mydata = {arr:[{
		
		name:"<?php echo $student->lastname?>",
		age:34},{name:"steven",age:36}]}
	})
	  $(document).ready(function(){
	

	  });


	  	$('#test').append("<?php $array =""; foreach($results as $s){$array.=$s->lastname.', '.$s->firstname.':'; } ?>");
	  
	  	var str = "<?php echo $array;?>";
	  	console.log("h>"+str);
	  	var result = str.split(":");
	  	console.log(result);
	  $(function() {
    $( "#tags" ).autocomplete({
      source: result
    });
  });
	  </script>

</body>
</html>