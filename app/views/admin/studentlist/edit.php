
<!DOCTYPE html>
<html>
<head>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
  <link rel="stylesheet" href="/resources/demos/style.css">

  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/foundation.css">
  <link rel="stylesheet" href="/css/createstudent.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="/assets/js/vendor/modernizr.js"></script>
   <link rel="icon" href="http://d15dxvojnvxp1x.cloudfront.net/assets/favicon.ico">
  <link rel="stylesheet" type="text/css" media="all" href="css/styles.css">

</head>
<body>

<div id="header">
  <div class="container">
    <img src="/img/CSBNLogo.png">
      <span id="CSBN">CSBN</span>
      <span id="portal">Portal</span>
      <p id="colegio">Colegio de San Bartolome de Novaliches</p>

       <a href="#" id="admins" data-dropdown="admin" class="tiny button dropdown"><?php echo "Admin"?></a><br>
        <ul id="admin" data-dropdown-content class="f-dropdown">
          <li><a href="/adminchangepass">Change Password</a></li>
          <li><a href="/admin-logout">Logout</a></li>
        </ul>
    </img>
  </div>
</div>
<div id="line"></div>

<ul class="side-nav">
  <li><a href="/studentlist">Student</a></li>
  <li><a href="/teacherlist">Teacher</a>
</ul>
<div id="maincontent">
 <ul class="breadcrumbs" id="bc">
  <li><a href="/studentlist">Main  &nbsp;&nbsp;> </a></li>
  <li><a href="/studentlist/create" class="current"> &nbsp;&nbsp;Edit Profile </li>
</ul><a class="button tiny secondary" type="button" href="/studentlist"> < Back</a>
 <br>
<?php if(Session::has('message')){?>
  <div class="alert alert-info"><?php Session::get('message');?></div>
<?php }?>

<!-- if there are creation errors, they will show here -->
<?php if($errors->has()){?>
              <div data-alert class="alert-box alert">
                <?php foreach ($errors->all() as $error) { ?>
                  <li><?php echo $error; ?></li>
                <?php } ?>
              </div>
            <?php } ?> 
            <?php  $message = Session::get('success'); if($message!=null){?>
            <div data-alert class="alert-box success">
                
                  <li><?php echo $message;?></li>
              
              </div>
              <?php }?>


  
<form action="/studentlist/<?php echo $student->id; ?>" method="POST" enctype="multipart/form-data">
<input type="hidden" name="_method" value="PUT">
    <table class="radius center large-10 medium-8 small-6 large-centered columns">
      
      <tr>
        
        <td><span id="required">*</span>Student Number:</td>
        <td>
          <input type="text" value="<?php echo $student->studentnumber;?>" name="studentnumber" id="studentnumber">
        </td>
        <td><span id="required">*</span>Grade/Year LeveL:</td>
        <td>
          <option value="<?php echo $student->level;?>" id="levell"></option>
          <select  name="level" id="level" onchange="" size="1">

            <option value="Preschool"> Preschool </option>
            <option value="Grade1"> Grade 1 </option>
            <option value="Grade2"> Grade 2 </option>
            <option value="Grade3"> Grade 3 </option>
            <option value="Grade4"> Grade 4</option>
            <option value="Grade5"> Grade 5 </option>
            <option value="Grade6"> Grade 6 </option>
            <option value="1stYear"> 1st Year </option>
            <option value="2ndYear"> 2nd Year </option>
            <option value="3rdYear"> 3rd Year </option>
            <option value="4thYear"> 4th Year </option>
           
          </select>
        </td>
      </tr>
      <tr>
        <td><span id="required">*</span>First Name:</td>
        <td>
          <input type="text" value="<?php echo $student->firstname;?>" name="firstname" id="firstname">
        </td>
        <td><span id="required">*</span>Last Name:</td>
        <td>
          <input type="text" value="<?php echo $student->lastname;?>" name="lastname" id="lastname">
        </td>
        
      </tr>
      <tr>
       <option value="<?php echo $student->gender;?>" id="genderr"></option>
        <td><span id="required">*</span>Gender:</td>
        <td>
          <input type="radio" name="gender" value="Male" id="Male"><label for="gender">Male</label>
          <input type="radio" name="gender" value="Female" id="Female"><label for="gender">Female</label>
        </td>
        <td>Nationality:</td>
        <td>
          <input type="text" value="<?php echo $student->nationality;?>" name="nationality" id="nationality">
        </td>

      </tr>
<tr>
        <td><span id="required">*</span>Birthday:</td>
        <td><input colspan="3"type="text" id="datepicker" name="birthday"></td>
        
         <!-- <select name="month" id="month" onchange="" size="1">
            <option value=""></option>
            <option value="01"> January </option>
            <option value="02"> February </option>
            <option value="03"> March </option>
            <option value="04"> April </option>
            <option value="05"> May </option>
            <option value="06"> June </option>
            <option value="07"> July </option>
            <option value="08"> August </option>
            <option value="09"> September </option>
            <option value="10"> October </option>
            <option value="11"> November </option>
            <option value="12"> December </option>
          </select>
          </td>
          <td>

          <select name="day" id="Day" onselect="day" size="1">
            <option value=""></option>
            <option value="01"> 01 </option>
            <option value="02"> 02 </option>
            <option value="03"> 03 </option>
            <option value="04"> 04 </option>
            <option value="05"> 05 </option>
            <option value="06"> 06 </option>
            <option value="07"> 07 </option>
            <option value="08"> 08 </option>
            <option value="09"> 09 </option>
            <option value="10"> 10 </option>
            <option value="11"> 11 </option>
            <option value="12"> 12 </option>
            <option value="13"> 13 </option>
            <option value="14"> 14 </option>
            <option value="15"> 15 </option>
            <option value="16"> 16 </option>
            <option value="17"> 17 </option>
            <option value="18"> 18 </option>
            <option value="19"> 19 </option>
            <option value="20"> 20 </option>
            <option value="21"> 21 </option>
            <option value="22"> 22 </option>
            <option value="23"> 23 </option>
            <option value="24"> 24 </option>
            <option value="25"> 25 </option>
            <option value="26"> 26 </option>
            <option value="27"> 27 </option>
            <option value="28"> 28 </option>
            <option value="29"> 29 </option>
            <option value="30"> 30 </option>
            <option value="31"> 31 </option>
          </select>
          </td>
          <td>
          <select id="year" name="year">
          <option value=""></option>
          <option value="2014">2014</option>
          <option value="2013">2013</option>
          <option value="2012">2012</option>
          <option value="2011">2011</option>
          <option value="2010">2010</option>
          <option value="2009">2009</option>
          <option value="2008">2008</option>
          <option value="2007">2007</option>
          <option value="2006">2006</option>
          <option value="2005">2005</option>
          <option value="2004">2004</option>
          <option value="2003">2003</option>
          <option value="2002">2002</option>
          <option value="2001">2001</option>
          <option value="2000">2000</option>
          <option value="1999">1999</option>
          <option value="1998">1998</option>
          <option value="1997">1997</option>
          <option value="1996">1996</option>
          <option value="1995">1995</option>
          <option value="1994">1994</option>
          <option value="1993">1993</option>
          <option value="1992">1992</option>
          <option value="1991">1991</option>
          <option value="1990">1990</option>
          </select>-->
        </td>
        
      </tr>
      <tr>
        <td>Address:</td>
        <td colspan="3">
          <input type="text" value="<?php echo $student->address;?>" name="address" id="address">
        </td>
      </tr>
      
      <tr>
        <td>Contact:</td>
        <td colspan="3">
          <input type="text" value="<?php echo $student->contact;?>" name="contact" id="contact">
        </td>
      </tr>
      <tr>
        <td>Email:</td>
        <td colspan="3">
          <input type="text" value="<?php echo $student->email;?>" name="email" id="email">
        </td>
      </tr>
      <tr>
        <th colspan="4">Parent Information</th>
      </tr>
      <tr>
        <td>Father:</td>
        <td colspan="3">
          <input type="text" value="<?php echo $student->father;?>" name="father" id="father">
        </td>
      </tr>
      <tr>
        <td>Mother:</td>
        <td colspan="3">
          <input type="text" value="<?php echo $student->mother;?>" name="mother" id="mother">
        </td>
      </tr>
      
      <tr>
        <td>Contact:</td>
        <td>
          <input type="text" value="<?php echo $student->parentcontact;?>" name="parentcontact" id="parentcontact">
        </td>
        <td>Subscribe to SMS?</td>
        <option value="<?php echo $student->parentsms;?>" id="parentsms"></option>
        <td>
          <input type="radio" name="parentsms" value="yes" id="yes"><label for="parentsms-yes">Yes</label>
          <input type="radio" name="parentsms" value="no" id="no"><label for="parentsms-no">No</label>
        </td>
      </tr>
      
      <tr>
        <td colspan="4">
            <input class="button expand" type="submit" value="Save" />
        </td>
      </tr>
    </table>
    </form>

</div>


 <script>
   $("#datepicker").datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: '1994:2014'
    });
</script>
<script src="/js/vendor/jquery.js"></script>
  <script src="/js/foundation.dropdown.js"></script>
  <script src="/js/foundation.min.js"></script>
  <script>
  $(document).foundation();
   $( document ).ready(function() {

    var optionValue = $('#monthh').val();
    $("#month").val(optionValue).find("option[value=" + optionValue +"]").attr('selected', true);

    var optionValue1 = $('#dayy').val();
    $("#day").val(optionValue1).find("option[value=" + optionValue1 +"]").attr('selected', true);

    var optionValue2 = $('#yearr').val();
    $("#year").val(optionValue2).find("option[value=" + optionValue2 +"]").attr('selected', true);

    var level = $('#levell').val();
    $("#level").find("option[value=" + level +"]").attr('selected', true);


    var gender = $('#genderr').val();
    $("#"+gender).attr('checked', true);
    var sms = $('#parentsms').val();
    $("#"+sms).attr('checked', true);
  });
     
</script>
</body>
</html>
