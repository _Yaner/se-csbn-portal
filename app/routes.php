<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
//ADMIN SECTION
Route::get('admin-create', function(){
	$admin 		= new Admin;
	$admin->username 	= 'admin@csbnportal.com';
	$admin->password 	= Hash::make('admin');
	$admin->save();

	return "Save";
});

Route::get('send', function(){
	
	Twilio::to('+639495273956')->message('CSBN Portal SMS ACTIVATED! Sent by Admin');
	return "Sent";
});

/////////////////////////////////////////////////////////////////////////////////////////
Route::get('/admin/login', function()
{

	return View::make('layouts.login.admin');


});
Route::post('admin/login','HomeController@doAdminLogin');

Route::group(array(['prefix' => 'admin'],'before'=>'admin'), function()
{
	Route::post('/sendnote','HomeController@sendnote');


Route::get('/adminchangepass','HomeController@adminchangepass');
Route::post('/adminchangepass','HomeController@adminupdatepass');
Route::post('/search','HomeController@search');
Route::post('/searchTeacher','HomeController@searchTeacher');


Route::get('/studentlist/{id}/grades','HomeController@adminviewgrades');
Route::get('/studentlist/{id}/attendance','HomeController@adminviewattendance');
Route::get('/studentlist/attendance/viewlogs','HomeController@adminviewlogs');
Route::get('/studentlist/{id}/reset','HomeController@adminstudentreset');

Route::get('/studentlist/{id}/addNote','HomeController@adminviewaddnote');

Route::get('/teacherlist/{id}/reset','HomeController@adminstudentreset');
	Route::resource('studentlist','StudentListController');
	Route::resource('teacherlist','TeacherListController');
	Route::get('/admin-logout',function(){
	Session::flash('success','Successfully logout');
		Session::flush();
		return Redirect::to('/admin/login');
	});

});
////////////////////////////////////////////////////////////////////////////////////////////


//STUDENT
/////////////////////////////////////////////////////////////////////////////////////////

Route::get('/student/login', function()
{
	return View::make('layouts.login.student');
});
Route::post('/login','HomeController@doLogin');
Route::group(array(['prefix' => 'student'],'before'=>'student'), function()
{
	Route::get('/','HomeController@index');

	Route::get('/changepass','HomeController@changepass');
	Route::post('/changepass','HomeController@editpass');
	Route::get('/editcontact','HomeController@geteditcontact');
	Route::post('/editcontact','HomeController@savecontact');

	
	Route::get('/grades', 'HomeController@viewgrades');


	Route::get('/attendance', function()
	{
		return View::make('layouts.student.attendance');
	});

	Route::get('/profile', 'HomeController@viewStudent');

	Route::get('/guidance', function()
	{
		return View::make('layouts.student.GuidanceNote');
	});
	Route::get('/student-logout',function(){
	Session::flash('success','Successfully logout');
		Session::flush();
		return Redirect::to('/admin/login');
});


});
///////TEACHER MODULE
//////////////////////////////////////////////////////////////
Route::get('/teacher/login', function()
{

	return View::make('layouts.login.teacher');


});
Route::post('/sendnote','HomeController@sendnote');

Route::post('teacher/login','HomeController@doTeacherLogin');
Route::get('/teacherchangepass','HomeController@tadminchangepass');
Route::post('/teacherchangepass','HomeController@tadminupdatepass');
Route::get('/teacher/{id}/grades','HomeController@tadminviewgrades');
Route::get('/teacher/{id}/attendance','HomeController@tadminviewattendance');
Route::get('/teacher/attendance/viewlogs','HomeController@tadminviewlogs');
Route::get('/teacher/{id}/reset','HomeController@tadminstudentreset');

Route::get('/teacher/{id}/addNote','HomeController@tadminviewaddnote');
Route::get('/addnote','HomeController@tadminviewaddnote');

Route::get('/teacher/{id}/reset','HomeController@adminstudentreset');
Route::group(array(['prefix' => 'teacher'],'before'=>'teacher'), function()
{
	Route::resource('teacher','TeacherController');
	Route::get('/teacher-logout',function(){
	Session::flash('success','Successfully logout');
		Session::flush();
		return Redirect::to('/teacher-login');
	});

});
Route::get('/sendmail',function(){
Mail::send('emails.auth.email',array('firstname'=>'Nin'), function($message) {
    $message->to('janinejaner@gmail.com', 'Janine Janer')->subject('testing');
});
 	return "sent";
});
/*
Route::post('admin/login','HomeController@doAdminLogin');
Route::get('/admin-logout',function(){
	Session::flash('success','Successfully logout');
		Session::flush();
		return Redirect::to('/admin-login');
});

Route::get('/student-logout',function(){
	Session::flash('success','Successfully logout');
		Session::flush();
		return Redirect::to('/login');
});
Route::get('/login', function()
{

	return View::make('layouts.login.student');


});
Route::post('/login','HomeController@doLogin');
// route to show the login form
//Route::get('login', array('uses' => 'HomeController@showLogin'));

	// route to process the form
//Route::post('login', array('uses' => 'HomeController@doLogin'));

Route::group(array(['prefix' => 'admin'],'before'=>'admin'), function()
{
	Route::resource('studentlists','StudentListController');
	Route::get('/students/addStudent', function()
	{
		return View::make('layouts.admin.admin');
	});
		Route::post('/admin/addStudent','profileAdminController@CreateStudent'); 

	Route::get('/students/list','profileAdminController@showstudentlist');
	Route::post('/students/list','profileAdminController@showlist');
	//Route::get('/students','profileAdminController@showlevels');
	Route::get('/students/list/view', 'profileAdminController@viewStudent');
});

Route::group(array(['prefix' => 'student'],'before'=>'student'), function()
{
	Route::get('/','HomeController@index');

	Route::get('/changepass','HomeController@changepass');
	Route::post('/changepass','HomeController@editpass');
	Route::get('/editcontact','HomeController@geteditcontact');
	Route::post('/editcontact','HomeController@savecontact');

	Route::get('/grades', function()
	{
		return View::make('layouts.student.grades');
	});


	Route::get('/attendance', function()
	{
		return View::make('layouts.student.attendance');
	});

	Route::get('/profile', 'HomeController@profile');

	Route::get('/guidance', function()
	{
		return View::make('layouts.student.GuidanceNote');
	});



});
/*Route::group(array('prefix' => 'admin','before'=>'admin'), function()
{
	
	Route::resource('reports','AdminReportController');
	Route::resource('users','AdminUserController');
	Route::get('logout', function(){
		Session::flush();
		return Redirect::to('/');
	});
});

*/
/*Route::get('login',function(){
	return 'Login';
});




	


Route::get('/logs', function()
{
	return View::make('layouts.student.logs.logs');
});




//Route::get('/profile','TestController@index');



Route::get('/teacher-profile', function()
{
	return View::make('layouts.teacher.profile');
});
Route::get('/addnote', function()
{
	return View::make('layouts.teacher.addnote');
});

//Test pages for manual user additions
//DO NOT TOUCH
Route::get('/addusermanual', function()
{
	return View::make('addusermanual');
});
Route::get('/addusermanualsuccess', function()
{
	return View::make('addusermanualsuccess');
});
	Route::get('/admin/addStudent','profileAdminController@showSignup');
	
---------------------------------------------------------------------


Route::get('/subjectgrade', function()
{
	return View::make('layouts.teacher.subjectgrade');
});

Route::get('/gradelevels', function()
{
	return View::make('layouts.teacher.gradelevels');
});

Route::get('/uploadgrade', function()
{
	return View::make('layouts.teacher.uploadgrade');
});

//Route::get('/profile','TestController@index');



Route::get('/teacher-profile', function()
{
	return View::make('layouts.teacher.profile');
});
Route::get('/addnote', function()
{
	return View::make('layouts.teacher.addnote');
});

//Test pages for manual user additions
//DO NOT TOUCH
Route::get('/addusermanual', function()
{
	return View::make('addusermanual');
});
Route::get('/addusermanualsuccess', function()
{
	return View::make('addusermanualsuccess');
});



Route::get('/admin-teachers', function()
{
	return View::make('layouts.admin.teachers');
});

Route::get('/parent', function()
{
	return View::make('layouts.parent.profile');
});
Route::get('/parent-grades', function()
{
	return View::make('layouts.parent.grades');
});
Route::get('/parent-attendance', function()
{
	return View::make('layouts.parent.attendance');
});
Route::get('/parent-guidance', function()
{
	return View::make('layouts.parent.GuidanceNote');
});