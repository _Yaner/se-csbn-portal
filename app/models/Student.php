<?php
use Jenssegers\Mongodb\Model as Eloquent;

class Student extends Eloquent {

    protected $collection = 'student';
    
}