<?php
use Jenssegers\Mongodb\Model as Eloquent;

class Teacher extends Eloquent {

    protected $collection = 'teacher';
    
}